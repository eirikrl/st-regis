;(function($,window,undefined){

    $(document).ready(function ($) {



        //responsive image maps for location page
        $('img[usemap]').rwdImageMaps();


        // location hover popovers
        var hasPopover = false;
        $('#location area').mouseenter(function() {

            var id = $(this).attr('id');
            var title = $(this).attr('data-content-title');
            var content = $(this).attr('data-content');
            var contentImg = $(this).attr('data-content-image');

            var coordsArr = $(this).attr('coords').split(",");
            var horOffset;
            var topOffset = parseInt(coordsArr[1]) - parseInt(coordsArr[2]);

            var $popOver;

            //position popover left or right of the area tag depending on where it is
            if(coordsArr[0] < $('#location-map').width()/2 ) {
              horOffset =  parseInt(coordsArr[0]) + parseInt(coordsArr[2]) + 20;
              $popOver = $('<div/>', {
                class: 'location-popover',
                style: "top:"+topOffset+"px; left:"+ horOffset + "px;"
                });
            }else {
                horOffset = $('#location-map').width() - parseInt(coordsArr[0]) + parseInt(coordsArr[2]) + 20;
                $popOver = $('<div/>', {
                class: 'location-popover',
                style: "top:"+topOffset+"px; right:"+ horOffset + "px;"
            });
            }
            var $title = $('<div/>', {
                class: 'location-popover-title'
            }).html(title);

            var $content = $('<div/>', {
                class: 'location-popover-content'
            }).append($title).append(content);



            var img = '<img src="'+contentImg+'" />';


            var $closeBtn = $('<button type="button" class="location-popover-close close" aria-hidden="true">&times;</button>');

            $closeBtn.click(function() {
                $(this).parent().remove();
            });

            $popOver.append(img);
            $popOver.append($closeBtn);
            $popOver.append($content);


            if(!hasPopover) {

                $(this).closest('div').append($popOver);
                hasPopover = true;
            }
        }).mouseleave(function() {

            $(this).closest('div').find('.location-popover').remove();
            hasPopover = false;
        });
        //end of location popover



    });


})(jQuery,this);


