;(function($,window,undefined){

    $(document).ready(function ($) {


        //logo shrinking
        // function checkViewPort() {
        //   if($(window).width() > 768)  return true;
        //   return false;
        // }
        // if(checkViewPort()) {
        //     $(window).scroll(function(e) {
        //         var distance = ($('section.slide').length > 0) ? $('section.slide').eq(1).offset().top : null;
        //         if($(window).scrollTop() > distance) {
        //             $('h1.logo').css('height', '60px');
        //         }else{
        //             $('h1.logo').css('height', '131px');
        //         }
        //     });
        // }


        $('.benefits-carousel').carousel({
          interval: false
        });

        var villaTypesCarousel = $('#villa-types-carousel').carousel({
            interval: false
        });


        //navigation arrow functionality
        $('.slide-arrow').click(function() {
            var nextSlideId = $(this).closest('section').next('section').attr('id');
            console.log(nextSlideId);
            goToByScroll(nextSlideId);
        });


        $('.villa-types').tooltip({
            html: true,
            placement: 'bottom',
            container: '.the-residences-villa-types'
        });


        $('body').scrollspy({ target: '.scrollspy-target' });

        var $links = $('.navbar-subnav').find('li'),
            $htmlbody = $('html,body');
        function goToByScroll(datasection) {
            $htmlbody.animate({
                scrollTop: $('.slide[id="' + datasection + '"]').offset().top
            }, 2000, 'easeInOutQuint');
        }
        //When the user clicks on the navigation links, get the data-section attribute value of the link and pass that variable to the goToByScroll function
        $links.click(function (e) {

            e.preventDefault();
            datasection = $(this).attr('data-section');

            //slide back to the first slide in carousel
            if($(this).attr('data-section') === 'villa-types') {
                villaTypesCarousel.carousel(0);
            }

            goToByScroll(datasection);
        });


        if($('.doorhandle-container').length > 0) {

            //distance from top of handles to browser window
            var viewableOffset = $("#doorhandle-l").offset().top - $(window).scrollTop();
            var open = 550-viewableOffset;
            //console.log(viewableOffset);

                if(viewableOffset < 550 && viewableOffset > 220) {
                    $('#doorhandle-l').css('right', open/2 + 'px' );
                    $('#doorhandle-r').css('left', open/2 + 'px' );
                }else if(viewableOffset <= 220) {
                    $('#doorhandle-l').css('right','170px' );
                    $('#doorhandle-r').css('left', '170px' );
                }

            $(window).scroll(function() {
                viewableOffset = $("#doorhandle-l").offset().top - $(window).scrollTop();
                //console.log(viewableOffset);
                var open = 550-viewableOffset;

                if(viewableOffset < 550 && viewableOffset >= 220) {
                    $('#doorhandle-l').css('right', open/2 + 'px' );
                    $('#doorhandle-r').css('left', open/2 + 'px' );
                }

                if(viewableOffset >= 550) {
                    $('#doorhandle-l').css('right', '0' );
                    $('#doorhandle-r').css('left', '0' );
                }
                if(viewableOffset < 220) {
                    $('#doorhandle-l').css('right', '170px' );
                    $('#doorhandle-r').css('left', '170px' );
                }

            });
        }


        //villa types slider
        $('.villa-types-bullets').hide();

        $('.villa-types, .villa-types-bullets li').click(function(e) {
            e.preventDefault();
            var $this = $(this); //preserving context
            var index = $this.hasClass('villa-types') ? $this.index() + 1 : $this.index();

            var url = $this.attr('data-content-source') + '?ajax=1&v=' + new Date().getTime();
            // console.log(url);

            //check if ajax loaded already
            if ( $('#villa-types-carousel .item').eq(index).is('[data-ajax-loaded]') === false ) {
                $.get(url, function(data) {
                    $('#villa-types-carousel .item').eq(index).attr('data-ajax-loaded','').html(data);
                    //if it's the hexagon linka, trigger the carousel bullet which will move the slide
                    if($this.hasClass('villa-types')) {
                        villaTypesCarousel.carousel(index);
                    }

                    $('.villa-types-image-zoom a').magnificPopup({
                        type: 'image',
                        closeOnContentClick: true,
                        closeBtnInside: false,
                        fixedContentPos: true,
                        mainClass: 'mfp-no-margins mfp-with-zoom', // class to remove default margin from left and right side
                        image: {
                            verticalFit: true
                        },
                        zoom: {
                            enabled: true,
                            duration: 300 // don't foget to change the duration also in CSS
                        }
                    });
                });

            }else {
                if($this.hasClass('villa-types')) {
                    villaTypesCarousel.carousel(index);
                }
            }
        });

        $('#villa-types-carousel').on('slide.bs.carousel', function (e) {
            //stupid event fires before new active class is attached to next element
            //http://stackoverflow.com/questions/9860436/twitter-bootstrap-carousel-access-current-index
            var nextSlideIndex = $(e.relatedTarget).index();
            if(nextSlideIndex === 0 ) {
              $('.villa-types-bullets').hide();
            }else {
              $('.villa-types-bullets').show();
            }
        });


        $('#benefits-tabs a').click(function (e) {
          e.preventDefault();
          $(this).tab('show');
        });
        $('#benefits-tabs a[href="#exclusive-tab"]').tab('show'); // Select first tab

    });


})(jQuery,this);


