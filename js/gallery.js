;(function($,window,undefined){

    var gallery = {

      $container: $('#gallery-container'),

      input: {
        type: '',
        designer: '',
        location: ''
      },

      init: function() {
        $(window).load(function (){
          gallery.$container.isotope({
            // options
            itemSelector : '.item',
            layoutMode : 'fitRows',
            animationEngine: 'best-available',//'jquery'
            onLayout: function( $elems, instance ) {
                $(window).trigger('scroll');
              }
          });
        });

        $(document).delegate('*[data-toggle="lightbox"]', 'click', function(event) {
          event.preventDefault();
          return $(this).ekkoLightbox();
        });

        $("img.lazy").lazyload({
          effect: "fadeIn",
          // http://stackoverflow.com/questions/11337291/combining-jquery-isotope-and-lazy-load
          failure_limit: Math.max($('img.lazy').length-1, 0)
        });


        $('#villa-type-filter').change(function(e){

          gallery.input.type = ($(this).val() === '') ? '' : '.type-'+$(this).val();
          gallery.$container.isotope({ filter: 'img'+gallery.input.type+gallery.input.designer+gallery.input.location});

        });

        $('#designer-filter').change(function(e){

          gallery.input.designer = ($(this).val() === '') ? '' : '.designer-'+$(this).val();
          gallery.$container.isotope({ filter: 'img'+gallery.input.type+gallery.input.designer+gallery.input.location});
        });

        $('#location-filter').change(function(e){

          gallery.input.location = ($(this).val() === '') ? '' : '.location-'+$(this).val();
          gallery.$container.isotope({ filter: 'img'+gallery.input.type+gallery.input.designer+gallery.input.location});
        });

      } //init

    };// gallery

    $(document).ready(function ($) {
      gallery.init();
    });

})(jQuery,this);