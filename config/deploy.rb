require 'mina/git'

set :domain, '106.187.103.145' # Alchemy Asia Development Server
set :deploy_to, '/home/sites/st-regis.alchemyasia.com/'
set :repository, 'git@bitbucket.org:alchemyasia/st-regis.git'
set :branch, 'master'

set :user, 'root'

task :environment do
end

# Put any custom mkdir's in here for when `mina setup` is ran.
task :setup => :environment do
end

desc "Deploys the current version to the server."
task :deploy => :environment do
  deploy do

    invoke :'git:clone'

  end
end