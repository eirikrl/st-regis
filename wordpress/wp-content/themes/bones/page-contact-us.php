<?php
/*
Template Name: Contact Us Template
*/
?>

<?php get_header(); ?>


<div id="main" role="main">
<?php
if (have_posts()) : while (have_posts()) : the_post();
$disclaimer = get_post_meta( $post->ID, '_contact_us_disclaimer', true );
$contact_address = get_post_meta( $post->ID, '_contact_us_address', true );
$contact_telephone = get_post_meta( $post->ID, '_contact_us_telephone', true );
$contact_email = get_post_meta( $post->ID, '_contact_us_email', true );
?>
  <section class="container content">
    <div class="row">

    <div class="contact-us-form col-xs-12 col-sm-4 col-sm-offset-4">
        <?php the_content(); ?>
    </div>
    <div class="col-xs-12 col-sm-3 col-sm-offset-1 contact-address">
      <div class="contact-address-title"><?php translation_strings('Address','地址') ?></div>
      <p><?php echo $contact_address; ?></p>
      <div class="contact-address-title"><?php translation_strings('Tel','电话号码')?></div>
      <p><?php echo $contact_telephone; ?></p>
      <div class="contact-address-title"><?php translation_strings('Email','电邮地址')?></div>
      <a href="mailto:<?php echo $contact_email; ?>"><?php echo $contact_email; ?></a>
    </div>
    </div>
    <div class="row">
      <div class="col-xs-12 disclaimer">
        <?php echo $disclaimer; ?>
      </div>
    </div>
  </section>
<?php endwhile; else : ?>
   <h1><?php _e( 'Oops, Post Not Found!', 'bonestheme' ); ?></h1>
<?php endif; ?>
</div>

<?php get_footer(); ?>
