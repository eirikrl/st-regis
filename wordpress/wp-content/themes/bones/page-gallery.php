<?php
/*
Template Name: Gallery Template
*/
?>

<?php get_header(); ?>



<?php
if (have_posts()) : while (have_posts()) : the_post();

$gallery_images = get_option(WCEPT_GALLERY_SLUG, array());

?>

<div id="role" main="role">
  <section class="container content">
    <div class="row">

      <div class="col-lg-12 gallery-filter-controls">
        <label for="villa-type-filter"><?php translation_strings('villa type','豪华物业')?>
        <select id="villa-type-filter">
          <option selected value=""><?php translation_strings('All Villas','所有豪华物业')?></option>
          <option value="shanqu"><?php translation_strings('Shan Qu','山趣')?></option>
          <option value="siqu"><?php translation_strings('Si Qu','思渠')?></option>
          <option value="lingyun"><?php translation_strings('Ling Yun','凌云')?></option>
          <option value="duixue"><?php translation_strings('Dui Xue','对雪')?></option>
          <option value="tianying"><?php translation_strings('Tian Ying','天影')?></option>
          <option value="clubhouse"><?php translation_strings('Clubhouse','会所')?></option>
        </select>
        <span class="caret"></span>
        </label>

        <label for="designer-filter"><?php translation_strings('designer','设计师')?>
        <select id="designer-filter">
          <option selected value=""><?php translation_strings('All Designers','所有设计师')?></option>
          <option value="CCD"><?php translation_strings('CCD','郑中')?></option>
          <option value="HBA"><?php translation_strings('HBA','赫斯贝德纳')?></option>
          <option value="KKD"><?php translation_strings('KKD','高文安')?></option>
          <option value="Steve_Leung"><?php translation_strings('Steve Leung','梁志天')?></option>
        </select>
        <span class="caret"></span>
        </label>

        <label for="location-filter"><?php translation_strings('location','位置')?>
        <select id="location-filter">
          <option selected value=""><?php translation_strings('All Locations','所有位置')?></option>
          <option value="bathrooms"><?php translation_strings('Bathrooms','卫生间')?></option>
          <option value="bedrooms"><?php translation_strings('Bedrooms','睡房')?></option>
          <option value="dining_area"><?php translation_strings('Dining area', '饭厅')?></option>
          <option value="kitchen"><?php translation_strings('Kitchen','厨房')?></option>
          <option value="living_room"><?php translation_strings('Living room','客厅')?></option>
          <option value="others"><?php translation_strings('Others','其他')?></option>

        </select>
        <span class="caret"></span>
        </label>
      </div>



    </div>

    <div class="row">
      <div class="col-xs-12">
        <div id="gallery-container" class="gallery-container">
            <?php
            if(!empty($gallery_images)){
                foreach($gallery_images as $img){
                    //$filters = _gallery_filters($gallery_filters, $uniqueID, $img['type'], $img['designer'], $img['location']);
                    $caption = translation_strings($img['caption'],$img['chinese_caption'], false);
                    echo '<a href="'.$img['full'].'" data-toggle="lightbox" data-footer="'.$caption.'" data-gallery="multiimages">
                    <img class="lazy item type-'.$img['type'].' designer-'.$img['designer'].' location-'.$img['location'].'" width="170" height="127" data-original="'.$img['thumb'].'" />
                    </a>';
                }
            }
            ?>

        </div>
      </div>
    </div>

  </section>
</div>
<?php endwhile; else : ?>
   <h1><?php _e( 'Oops, Post Not Found!', 'bonestheme' ); ?></h1>
<?php endif; ?>


<?php get_footer(); ?>
