<?php
/*
Template Name: The Residence Index Template
*/
?>

<?php get_header();

?>


<div id="main" role="main">

    <?php
    $fp_sections = new WP_Query(array('post_type' => 'fp_section', 'order' => 'ASC', 'orderby' => 'menu_order', 'posts_per_page' => -1));
    $villa_types = new WP_Query(array('post_type' => 'villa_type', 'order' => 'ASC', 'orderby' => 'menu_order', 'posts_per_page' => -1));

    while ($fp_sections->have_posts()) : $fp_sections->the_post();
    $section_template_id = get_post_meta( $post->ID, 'section_template_id', true );
    ?>
    <?php

    switch ($section_template_id) {
      case 'intro':

    ?>
    <section id="<?php echo $section_template_id;/*$post->post_name;*/ ?>" class="slide the-residences-<?php echo $section_template_id?>">
      <div class="container content">
        <div class="row">
          <div class="col-xs-12">
            <img class="intro-image" src="<?php echo get_template_directory_uri()?>/library/images/intro-hexagon.png" />
            <div class="intro-slogan"><?php the_content(); ?></div>
            <a class="slide-arrow intro-arrow"href="#">v</a>
          </div>
        </div>
      </div>
    </section>
    <?php
        break;

      case 'overview':
    ?>
    <section id="<?php echo $section_template_id;/*$post->post_name;*/ ?>" class="slide the-residences-<?php echo $section_template_id?>">
      <div class="container content">
        <div class="row doorhandle-background">
          <div class="col-xs-6 doorhandle-container">
            <div class="doorhandle doorhandle-l hidden-xs" id="doorhandle-l">
            </div>
            <div class="doorhandle-copy">
              <?php the_content(); ?>
            </div>
          </div>
          <div class="col-xs-6">
            <div class="doorhandle doorhandle-r hidden-xs" id="doorhandle-r"></div>
          </div>
        </div>
      </div>
    </section>
    <?php

        break;
      case 'location':
    ?>
    <section id="<?php echo $section_template_id;/*$post->post_name;*/ ?>" class="slide the-residences-<?php echo $section_template_id?>">
      <div class="container content">
        <div class="row">
          <div class="col-xs-12">
            <?php
              if(get_language() == 'zh') {
                $map = '/library/images/location-map-zh.png';
                $mapPortrait = '/library/images/location-map-portrait-zh.png';
              }else{
                $map = '/library/images/location-map.png';
                $mapPortrait = '/library/images/location-map-portrait.png';
              }
            ?>
            <img src="<?php echo get_template_directory_uri();echo $map;?>" usemap="#location" border="0" id="location-map" class="hidden-sm hidden-xs" />
            <img src="<?php echo get_template_directory_uri();echo $mapPortrait; ?>" usemap="#location" border="0" id="location-map-portrait" class="visible-sm visible-xs" />
          </div>
        </div>
      </div>
    </section>
    <?php
        break;
      case 'villa-types':
    ?>
    <section id="<?php echo $section_template_id;/*$post->post_name;*/ ?>" class="slide the-residences-<?php echo $section_template_id?>">
      <div id="villa-types-carousel" class="carousel slide" data-ride="carousel">
        <!-- Wrapper for slides -->
        <div class="carousel-inner">
          <div class="item active">
            <div class="carousel-caption">
              <div class="container content">
                <?php while ($villa_types->have_posts()) : $villa_types->the_post();
                  $specs = get_post_meta( $post->ID, '_villa_type_specs', true );
                  echo '<a href="#" data-toggle="tooltip" title="'.$specs.'" class="villa-types villa-types-'.$post->post_name.'" data-content-source="../villa_type/'.$post->post_name.'" ></a>';
                  endwhile;
                ?>
              </div>
            </div>
          </div>
          <?php while ($villa_types->have_posts()) : $villa_types->the_post();
          //added pathing change due to polylang plugin
          echo '<div class="item" data-content-source="../villa_type/'.$post->post_name.'"></div>';
          endwhile;
          ?>
        </div>

        <div class="container villa-types-bullets-container">
          <div class="row">
            <div class="col-xs-9 col-xs-offset-3" style="position:relative;">
              <!-- Indicators -->
              <ol class="carousel-indicators villa-types-bullets">
                <li data-target="#villa-types-carousel" data-slide-to="0" class="active hidden" >
                </li>
                <?php
                $c = 1;
                while ($villa_types->have_posts()) : $villa_types->the_post();
                echo '<li data-target="#villa-types-carousel" data-slide-to="'.$c.'" class="'.$post->post_name.'" data-content-source="../villa_type/'.$post->post_name.'"><span>'.$post->post_title.'</span></li>';
                $c++;
                endwhile;
                ?>
              </ol>
            </div>
          </div>
        </div>
      </div>
    </section>
    <?php
        break;
      case 'benefits':

      $exclusive_tab = get_post_meta( $post->ID, '_benefits_exclusive', true );
      $customized_tab = get_post_meta( $post->ID, '_benefits_customized', true );

    ?>
    <section id="<?php echo $section_template_id;/*$post->post_name;*/ ?>" class="slide the-residences-<?php echo $section_template_id?>">
      <div class="butler-bg hidden-xs hidden-sm"></div>
      <div class="container">
        <div class="row">
          <div class="col-xs-12">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs benefits-tabs" id="benefits-tabs">
              <li><a href="#customized-tab">
                <?php
                translation_strings('Customized Services for Owners', '业主私人定制化服务');
                ?>
              </a></li>
              <li><a href="#exclusive-tab">
                <?php
                translation_strings('Exclusive Services for Owners', '业主专享服务');
                ?>
              </a></li>
            </ul>
            <!-- Tab panes -->
            <div class="benefits-tab-content tab-content content">
              <div class="tab-pane" id="exclusive-tab">
                  <div class="row">
                    <div class="col-md-4 hidden-sm benefits-tab-content-left">
                      <img src="<?php echo get_template_directory_uri()?>/library/images/exclusive-service1.jpg" />
                    </div>
                    <div class="col-md-8 benefits-tab-content-right">

                        <div class="panel-group accordion-benefits" id="accordion-exclusive">
                        <?php echo do_shortcode($exclusive_tab) ?>
                        </div>
                    </div> <!-- /div.col-md-8 -->
                  </div> <!-- /div.row -->
              </div>
              <div class="tab-pane" id="customized-tab">
                  <div class="row">
                     <div class="col-md-4 hidden-sm benefits-tab-content-left">
                      <img src="<?php echo get_template_directory_uri()?>/library/images/customized-service1.jpg" />
                    </div>
                    <div class="col-md-8">

                        <div class="panel-group accordion-benefits" id="accordion-customized">
                        <?php echo do_shortcode($customized_tab) ?>
                        </div>
                    </div> <!-- /div.col-md-8 -->
                  </div> <!-- /div.row -->

              </div>
            </div>

          </div>
        </div><!-- /div.col-xs-12 -->
      </div> <!-- /div.container-->
    </section>
    <?php
      break;
    }
    ?>

        <?php endwhile; ?> <!-- END THE LOOP -->
    <?php wp_reset_postdata(); ?>

</div>



<?php get_footer(); ?>
