<?php require_once('partials/header.php'); ?>


<script type="text/javascript">
var gallery_filters = <?php echo $gallery_filters_json; ?>;
</script>

<div class="wcept-add-gallery-container">
	<ul class="wcept-gallery-forms">
		<li class="wcept-album-form<?php echo (!isset($wcept_gallery_type) || (isset($wcept_gallery_type) && $wcept_gallery_type == 'album'))? ' active': '' ?>">
			<div class="wcept-uploader">
                <input type="button" class="button" name="_unique_name_button" id="_unique_name_button" value="Add Image" />
            </div>
            <div id="gallery_images">
            <?php
            if(!empty($gallery_images)){

                foreach($gallery_images as $img){
                    $uniqueID = uniqid('image_id_');
                    $filters = _gallery_filters($gallery_filters, $uniqueID, $img['type'], $img['designer'], $img['location']);
                    echo '<div class="gallery_image_block" id="'.$uniqueID.'">
                    <img src="'.$img['full'].'" />
                    <a href="#" class="remove_gallery_image" attr="'.$uniqueID.'">remove image</a>
                    <div class="gallery_fields">'
                    .$filters.'
                    <br />
                    <label><h2>Caption</h2></lable><textarea rows="2" cols=80" name="gallery_images['.$uniqueID.'][caption]" type="text">'.$img['caption'].'</textarea>
                    <label><h2>标题</h2></lable><textarea rows="2" cols=80" name="gallery_images['.$uniqueID.'][chinese_caption]" type="text">'.$img['chinese_caption'].'</textarea>
                    </div>
                    <input type="hidden" name="gallery_images['.$uniqueID.'][full]" value="'.$img['full'].'" />
                    <input type="hidden" name="gallery_images['.$uniqueID.'][thumb]" value="'.$img['thumb'].'" />
                    </div>';
                }
            }
            ?>
            </div>
		</li>
	</ul>
	<input type="hidden" name="WCEPT_SUBMIT" id="WCEPT_SUBMIT" value="submit" />
	<input class="button-primary" type="submit" name="Save" value="Save" id="submitbutton" />
</div>
<?php require_once('partials/footer.php'); ?>