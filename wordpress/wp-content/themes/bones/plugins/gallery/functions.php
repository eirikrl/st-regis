<?php

add_action('wp_loaded', 'wcept_process_gallery_form');

function wcept_process_gallery_form() {
	$page = isset($_GET['page'])? $_GET['page']: '';

	if($page == WCEPT_GALLERY_SLUG && $page != ''){

		if($page == WCEPT_GALLERY_SLUG){
			if(isset($_POST['WCEPT_SUBMIT']) && $_POST['WCEPT_SUBMIT'] == 'submit'){
				$isSubmit = true;
			}else if(!isset($_POST['WCEPT_SUBMIT'])){
				$isSubmit = null;
			}else{
				$isSubmit = false;
			}

			if($isSubmit !== null) {
				if($fn == 'edit' && $gid == ''){
					wcept_set_gallery_message('error', 'Gallery not found.');

			    }else if($isSubmit === true){
			    	$error = false;

			    	$gallery_images = isset($_POST['gallery_images'])? $_POST['gallery_images']: array();
			    	$data = array( 'content' => serialize($gallery_images) );

			    	if(!$error){
			    		update_option(WCEPT_GALLERY_SLUG, $gallery_images);

			    		wcept_set_gallery_message('update', 'Gallery updated.');
			    	}
			    }

			    wp_redirect( admin_url('admin.php?page='.WCEPT_GALLERY_SLUG.'&message=1') );
			}
		}
	}
}

function wcept_enqueue_gallery_scripts(){
	wp_enqueue_script("gallery_admin_script", get_template_directory_uri()."/plugins/gallery/js/admin_script.js", array('jquery'));

	wp_register_style('admin-gallery-style', get_template_directory_uri() . '/plugins/gallery/css/gallery-admin.css');
  wp_enqueue_style( 'admin-gallery-style' );
}

function wcept_gallery_messages(){
	$message = isset($_GET['message'])? $_GET['message']: 0;
	$wcept_msg_type = get_option(WCEPT_MESSAGE_TYPE);
	$wcept_message = get_option(WCEPT_MESSAGE);

	if($wcept_msg_type !== false && $wcept_message !== false && $message){
?>
		<div class="<?php echo $wcept_msg_type; ?>">
			<p><?php _e( $wcept_message, 'micaheli_theme' ); ?></p>
	    </div>
<?php
		//wcept_set_gallery_message();
	}
}

function wcept_set_gallery_message($type = '', $message = ''){
	update_option(WCEPT_MESSAGE_TYPE, $type);
	update_option(WCEPT_MESSAGE, $message);
}