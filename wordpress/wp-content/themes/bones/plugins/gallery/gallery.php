<?php

define('WCEPT_GALLERY_SLUG', 'villa-gallery-index');
define('WCEPT_GALLERY_ADD_SLUG', 'villa-gallery-manage');

define('WCEPT_MESSAGE_TYPE', 'wcept_msg_type');
define('WCEPT_MESSAGE', 'wcept_message');

include_once('functions.php');

function wcept_gallery_menu_init() {
    //add_menu_page( $page_title, $menu_title, $capability, $menu_slug, $function, $icon_url, $position );
    add_menu_page('Villa Gallery', 'Villa Gallery', 'manage_options', WCEPT_GALLERY_SLUG, 'wcept_manage_gallery', '', '20.1313');

    //add_submenu_page( $parent_slug, $page_title, $menu_title, $capability, $menu_slug, $function );
    //add_submenu_page( WCEPT_GALLERY_SLUG, 'Add Gallery', 'Add Gallery', 'manage_options', WCEPT_GALLERY_ADD_SLUG, 'wcept_manage_gallery' );
}
add_action( 'admin_menu', 'wcept_gallery_menu_init' );

function wcept_manage_gallery(){
    wp_enqueue_media();
    wcept_enqueue_gallery_scripts();

    $gallery_filters_json = '{
        "type": [
            {"name": "Shan Qu", "value": "shanqu"},
            {"name": "Si Qu", "value": "siqu"},
            {"name": "Ling Yun", "value": "lingyun"},
            {"name": "Dui Xue", "value": "duixue"},
            {"name": "Tian Ying", "value": "tianying"},
            {"name": "Club House", "value": "clubhouse"}
            ],
        "designer": [
            {"name": "CCD", "value": "CCD"},
            {"name": "HBA", "value": "HBA"},
            {"name": "KKD", "value": "KKD"},
            {"name": "Steve Leung", "value": "Steve_Leung"}
            ],
        "location": [
            {"name": "Bathrooms", "value": "bathrooms"},
            {"name": "Bedrooms", "value": "bedrooms"},
            {"name": "Dining area", "value": "dining_area"},
            {"name": "Kitchen", "value": "kitchen"},
            {"name": "Living room", "value": "living_room"},
            {"name": "Others", "value": "others"}
            ]
    }';



    $gallery_filters = json_decode($gallery_filters_json);

    $title = 'Gallery';

    $gallery_images = get_option(WCEPT_GALLERY_SLUG, array());

	include_once('views/gallery-manage.php');
}

function _gallery_filters($gallery_filters, $rand_id = '', $type = '', $designer = '', $location = ''){
    $html = '';
    foreach($gallery_filters as $i => $v){
        $checked_value = '';

        $html .= '<h2>';
        switch($i){
            case 'type':
                $html .= 'villa type';
                $checked_value = $type;
                break;
            case 'designer':
                $html .= $i;
                $checked_value = $designer;
                break;
            case 'location':
                $html .= $i;
                $checked_value = $location;
                break;
        }
        $html .= '</h2>';

        foreach($v as $val){
            $html .= '<label><input type="radio" name="gallery_images['.$rand_id.']['.$i.']" value="'.$val->value.'" '.($checked_value == $val->value? 'checked': '').' /> '.$val->name.'</label>';
        }
    }

    return $html;
}

function _title_button($href = '', $text = ''){
    return ' <a class="add-new-h2 wcept-gallery-add" href="'.$href.'">'.$text.'</a>';
}