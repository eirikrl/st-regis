(function($) {
    $(function(){
        var _custom_media = true,
        _orig_send_attachment = wp.media.editor.send.attachment;

        $('#_unique_name_button').click(function(e) {
            var send_attachment_bkp = wp.media.editor.send.attachment;
            var button = $(this);
            //var id = button.attr('id').replace('_button', '');
            var id = 'gallery_images';

            _custom_media = true;
            wp.media.editor.send.attachment = function(props, attachment){
                if ( _custom_media ) {
                    $("#"+id).append( insert_image(attachment.sizes.full.url, (typeof(attachment.sizes.medium) != 'undefined' && typeof(attachment.sizes.medium.url) != 'undefined')? attachment.sizes.medium.url: attachment.sizes.full.url) );
                } else {
                    //return _orig_send_attachment.apply( this, [props, attachment] );
                }
            };
            wp.media.editor.open(button);

            return false;
        });

        $('.add_media').on('click', function(){
            _custom_media = false;
        });

        function insert_image(fullurl, thumburl) {
            var rand_id = 'image_id_'+Math.floor( Math.random() * 100000 ) + 1;
            var filters = _gallery_filters(rand_id);
            return '<div class="gallery_image_block" id="'+rand_id+'"><img src="'+fullurl+'" /><a href="#" class="remove_gallery_image" attr="'+rand_id+'">remove image</a>'+filters+'<input type="hidden" name="gallery_images['+rand_id+'][full]" value="'+fullurl+'" /><h2>Caption</h2><textarea name="gallery_images['+rand_id+'][caption]" type="text"></textarea><h2>标题</h2><textarea name="gallery_images['+rand_id+'][chinese_caption]" type="text"></textarea><input type="hidden" name="gallery_images['+rand_id+'][thumb]" value="'+thumburl+'" /></div>';
        }

        function _gallery_filters(rand_id){
            var html = '';

            $.each(gallery_filters, function(i, v){
                html += '<h2>';
                switch(i){
                    case 'type':
                        html += 'villa type';
                        break;
                    case 'designer':
                    case 'location':
                        html += i;
                        break;
                }
                html += '</h2>';

                $.each(v, function(j, val){
                    html += '<label><input type="radio" name="gallery_images['+rand_id+']['+i+']" value="'+val.value+'" /> '+val.name+'</label>';
                });
            });

            return html;
        }

        $(document).on('click', '.remove_gallery_image', function(e){
            e.preventDefault();

            var target = $(this).attr('attr');
            $('#'+target).remove();
        });

        $('input[name="wcept-gallery-type"]').change(function(){
            var value = $(this).val();

            $('ul.wcept-gallery-forms li').removeClass('active');
            $('.wcept-'+value+'-form').addClass('active');
        });

        $('#gallery_images').sortable();

        $('#wcept-gallery-list tbody').sortable({
            items: '.wcept-sort-items',
            handle: '.wcept-handle',
            update: function(ev, ui){
                var ids = [];
                $(this).find('tr .gallery_sortable_id').each(function(){
                    ids.push($(this).val());
                });

                $.ajax({
                    type: 'POST',
                    url: window.location.href,
                    data: {'ids': ids, isAjax: 1, fn: 'list_sort'},
                    dataType: 'json'
                }).done(function(data){
                    if(data.success){
                        // $('.toolbar-placeholder').before(
                        //     '<div class="conf"><span style="float:right"><a href="" id="hideWarn"><img src="../img/admin/close.png" alt="X"></a></span>'+data.message+'</div>'
                        // );
                    }
                });
            }
        });

        $('.confirm').click(function(e){
            return confirm("You are about to permanently delete the selected item.\n'Cancel' to stop, 'OK' to delete.");
        });
    });

})(jQuery);