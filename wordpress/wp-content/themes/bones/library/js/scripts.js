;(function($,window,undefined){

  $(document).ready(function ($) {

    $('body').scrollspy({ target: '.scrollspy-target' });

    var $links = $('.navbar-subnav').find('li'),
      $htmlbody = $('html,body');

    function goToByScroll(datasection) {
      $htmlbody.animate({
        scrollTop: $('.slide[id="' + datasection + '"]').offset().top
      }, 2000, 'easeInOutQuint');
     }

    //When the user clicks on the navigation links, get the data-section attribute value of the link and pass that variable to the goToByScroll function
    $links.click(function (e) {
      e.preventDefault();
      datasection = $(this).attr('data-section');

      goToByScroll(datasection);
    });

    //logo shrinking and hiding
    // var distance = ($('section.slide').length > 0) ? $('section.slide').eq(1).offset().top : $(window).height;

    // $(window).scroll(function(e) {
    //   if($(window).scrollTop() > distance) {
    //     $('h1.logo').css('height', '60px');
    //   }else{
    //     $('h1.logo').css('height', '131px');
    //   }
    // });

  });
})(jQuery,this);


