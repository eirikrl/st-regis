;(function($){
  $(function() {

     /** All metabox show/hide conditions go here **/


    //regular pages
    var pageTemplate = ($('#page_template').length > 0) ? $('#page_template').val() : null;
    if(pageTemplate !== 'page-contact-us.php') {
      $('#extra-information').hide();
    }

    //custom post types
    if($('body').hasClass('post-type-fp_section')) {

      var sectionTemplateId = $('#section-template-id').val();

      if(sectionTemplateId !== 'benefits') {
        $('#benefits-tabs').hide();
      }


    }



  });
})(jQuery);