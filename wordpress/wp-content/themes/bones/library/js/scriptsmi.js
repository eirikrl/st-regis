;(function($,window,undefined){

  $(document).ready(function ($) {

    //logo shrinking and hiding the residence subnav on first slide
    $('.navbar-subnav').eq(0).css('visibility','hidden');
    var distance = ($('section.slide').length > 0) ? $('section.slide').eq(1).offset().top : $(window).height;
    $(window).scroll(function(e) {
      if($(window).scrollTop() > distance) {
        $('.navbar-subnav').eq(0).css('visibility','visible');
      }else{
        $('.navbar-subnav').eq(0).css('visibility','hidden');
      }
    });


    $('.benefits-carousel').carousel({interval: false});

    var villaTypesCarousel = $('#villa-types-carousel').carousel({interval: false});

    var $links = $('.navbar-subnav').find('li');
    //When the user clicks on the navigation links, get the data-section attribute value of the link and pass that variable to the goToByScroll function
    $links.click(function (e) {
      e.preventDefault();
      datasection = $(this).attr('data-section');
      //slide back to the first slide in carousel
      if($(this).attr('data-section') === 'villa-types') {
        villaTypesCarousel.carousel(0);
      }
    });



    //responsive image maps for location page
    $('img[usemap]').rwdImageMaps();

    //navigation arrow functionality
    $('.slide-arrow').click(function(e) {
      e.preventDefault();
      var nextSlideId = $(this).closest('section').next('section').attr('id');
      console.log(nextSlideId);
      goToByScroll(nextSlideId);
    });

    // location hover popovers
    var hasPopover = false;
    $('#location area').mouseenter(function() {

      var id = $(this).attr('id');
      var title = $(this).attr('data-content-title');
      var content = $(this).attr('data-content');
      var contentImg = $(this).attr('data-content-image');

      var coordsArr = $(this).attr('coords').split(",");
      var horOffset;
      var topOffset = parseInt(coordsArr[1]) - parseInt(coordsArr[2]);

      var $popOver;

      //position popover left or right of the area tag depending on where it is
      if(coordsArr[0] < $('#location-map').width()/2 ) {
        horOffset =  parseInt(coordsArr[0]) + parseInt(coordsArr[2]) + 20;
        $popOver = $('<div/>', {
        class: 'location-popover',
        style: "top:"+topOffset+"px; left:"+ horOffset + "px;"
        });
      }else {
        horOffset = $('#location-map').width() - parseInt(coordsArr[0]) + parseInt(coordsArr[2]) + 20;
        $popOver = $('<div/>', {
        class: 'location-popover',
        style: "top:"+topOffset+"px; right:"+ horOffset + "px;"
      });
      }
      var $title = $('<div/>', {
        class: 'location-popover-title'
      }).html(title);

      var $content = $('<div/>', {
        class: 'location-popover-content'
      }).append($title).append(content);



      var img = '<img src="'+contentImg+'" />';


      var $closeBtn = $('<button type="button" class="location-popover-close close" aria-hidden="true">&times;</button>');

      $closeBtn.click(function() {
        $(this).parent().remove();
      });

      $popOver.append(img);
      $popOver.append($closeBtn);
      $popOver.append($content);


      if(!hasPopover) {

        $(this).closest('div').append($popOver);
        hasPopover = true;
      }
    }).mouseleave(function() {

      $(this).closest('div').find('.location-popover').remove();
      hasPopover = false;
    });
    //end of location popover


    $('.villa-types').tooltip({
      html: true,
      placement: 'bottom',
      container: '.the-residences-villa-types'
    });


    $('body').scrollspy({ target: '.scrollspy-target' });

    var $links = $('.navbar-nav li:first-child .navbar-subnav').find('li'),
      $htmlbody = $('html,body');
    function goToByScroll(datasection) {
      $htmlbody.animate({
        scrollTop: $('.slide[id="' + datasection + '"]').offset().top
      }, 2000, 'easeInOutQuint');
    }
    //When the user clicks on the navigation links, get the data-section attribute value of the link and pass that variable to the goToByScroll function
    $links.click(function (e) {
      e.preventDefault();
      datasection = $(this).attr('data-section');
      //slide back to the first slide in carousel
      if($(this).attr('data-section') === 'villa-types') {
        villaTypesCarousel.carousel(0);
      }
      goToByScroll(datasection);
    });


    function openDoorHandles () {

      var distanceToTop = $("#doorhandle-l").offset().top - $(window).scrollTop();
      var startOpenDistance = 550;
      var stopOpenDistance = 220;
      var open = startOpenDistance-distanceToTop;

      if(distanceToTop < startOpenDistance && distanceToTop > stopOpenDistance) {
        $('#doorhandle-l').css('right', open/2 + 'px' );
        $('#doorhandle-r').css('left', open/2 + 'px' );
      }else if(distanceToTop <= stopOpenDistance) {
        $('#doorhandle-l').css('right','170px' );
        $('#doorhandle-r').css('left', '170px' );
      }else if(distanceToTop >= startOpenDistance) {
        $('#doorhandle-l').css('right', '0' );
        $('#doorhandle-r').css('left', '0' );
      }
    }

    if($('.doorhandle-container').length > 0) {
      $(window).scroll(function() {
        openDoorHandles();
      });
    }


    //villa types slider
    $('.villa-types-bullets').hide();


    function getVillaTypes() {

      var retryCount = 0;
      $('#villa-types-carousel .item:not(:first)').each(function(e) {
        var $this = $(this); //preserving context

        var url = $this.attr('data-content-source') + '?ajax=1';
        //check if ajax loaded already
        if ( $(this).is('[data-ajax-loaded]') === false ) {
          var jqxhr = $.get(url, function(data) {
            $this.attr('data-ajax-loaded','').html(data);

            $('.villa-types-image-zoom a').magnificPopup({
              type: 'image',
              closeOnContentClick: true,
              closeBtnInside: false,
              fixedContentPos: true,
              mainClass: 'mfp-no-margins mfp-with-zoom', // class to remove default margin from left and right side
              image: {
                verticalFit: true
              },
              zoom: {
                enabled: true,
                duration: 300 // don't foget to change the duration also in CSS
              }
            });
          }).done(function() {

          }).fail(function(xhr, textStatus, errorThrown){
            retryCount++;
            //console.log(textStatus + ' plus ' + errorThrown);
            if(retryCount <=5) setTimeout(getVillaTypes, 1500);
          });
        }
      });

    }

    getVillaTypes();

    $('.villa-types').click(function(e) {
      e.preventDefault();
      villaTypesCarousel.carousel($(this).index() + 1);

    });


    // $('.villa-types, .villa-types-bullets li').click(function(e) {
    //   e.preventDefault();
    //   var $this = $(this); //preserving context
    //   var index = $this.hasClass('villa-types') ? $this.index() + 1 : $this.index();

    //   var url = $this.attr('data-content-source') + '?ajax=1&v=' + new Date().getTime();
    //   // console.log(url);


    //   //check if ajax loaded already
    //   if ( $('#villa-types-carousel .item').eq(index).is('[data-ajax-loaded]') === false ) {
    //     $.get(url, function(data) {
    //       console.log('sucess');
    //     }).done(function(data) {
    //        $('#villa-types-carousel .item').eq(index).attr('data-ajax-loaded','').html(data);

    //       //if it's the hexagon linka, trigger the carousel bullet which will move the slide
    //       if($this.hasClass('villa-types')) {
    //         villaTypesCarousel.carousel(index);
    //       }

    //       $('.villa-types-image-zoom a').magnificPopup({
    //         type: 'image',
    //         closeOnContentClick: true,
    //         closeBtnInside: false,
    //         fixedContentPos: true,
    //         mainClass: 'mfp-no-margins mfp-with-zoom', // class to remove default margin from left and right side
    //         image: {
    //           verticalFit: true
    //         },
    //         zoom: {
    //           enabled: true,
    //           duration: 300 // don't foget to change the duration also in CSS
    //         }
    //       });
    //     });

    //   }else {
    //     if($this.hasClass('villa-types')) {
    //       villaTypesCarousel.carousel(index);
    //     }
    //   }
    // });

    //showing villa bullets after initial slide
    $('#villa-types-carousel').on('slide.bs.carousel', function (e) {
      //stupid event fires before new active class is attached to next element
      //http://stackoverflow.com/questions/9860436/twitter-bootstrap-carousel-access-current-index
      var nextSlideIndex = $(e.relatedTarget).index();
      if(nextSlideIndex === 0 ) {
        $('.villa-types-bullets').hide();
      }else {
        $('.villa-types-bullets').show();
      }
    });


    $('#benefits-tabs a').click(function (e) {
      e.preventDefault();
      $(this).tab('show');
    });
    $('#benefits-tabs a[href="#exclusive-tab"]').tab('show'); // Select first tab

  });

  function pageInit() {

  }

  pageInit();


})(jQuery,this);


