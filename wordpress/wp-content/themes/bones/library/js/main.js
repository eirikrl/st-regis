;(function($,window,undefined){




  $(document).ready(function ($) {

    //logo shrinking and hiding the residence subnav on first slide
    $('.navbar-subnav').eq(0).css('visibility','hidden');
    var distance = ($('section.slide').length > 0) ? $('section.slide').eq(1).offset().top : $(window).height;
    $(window).scroll(function(e) {
      if($(window).scrollTop() > distance) {
        $('.navbar-subnav').eq(0).css('visibility','visible');
      }else{
        $('.navbar-subnav').eq(0).css('visibility','hidden');
      }
    });


    $('.villa-types-bullets').hide();

    $('.benefits-carousel').carousel({interval: false});

    var villaTypesCarousel = $('#villa-types-carousel').carousel({interval: false});

    var $links = $('.navbar-subnav').find('li');
    //When the user clicks on the navigation links, get the data-section attribute value of the link and pass that variable to the goToByScroll function
    $links.click(function (e) {
      e.preventDefault();
      datasection = $(this).attr('data-section');
      //slide back to the first slide in carousel
      if($(this).attr('data-section') === 'villa-types') {
        villaTypesCarousel.carousel(0);
      }
    });


    //navigation arrow functionality
    $('.slide-arrow').click(function(e) {
      e.preventDefault();
      var nextSlideId = $(this).closest('section').next('section').attr('id');
      goToByScroll(nextSlideId);
    });


    $('.villa-types').tooltip({
      html: true,
      placement: 'bottom',
      container: '.the-residences-villa-types'
    });


    $('body').scrollspy({ target: '.scrollspy-target' });

    var $links = $('.navbar-nav li:first-child .navbar-subnav').find('li'),
      $htmlbody = $('html,body');
    function goToByScroll(datasection) {
      $htmlbody.animate({
        scrollTop: $('.slide[id="' + datasection + '"]').offset().top
      }, 2000, 'easeInOutQuint');
    }
    //When the user clicks on the navigation links, get the data-section attribute value of the link and pass that variable to the goToByScroll function
    $links.click(function (e) {
      e.preventDefault();
      datasection = $(this).attr('data-section');
      //slide back to the first slide in carousel
      if($(this).attr('data-section') === 'villa-types') {
        villaTypesCarousel.carousel(0);
      }
      goToByScroll(datasection);
    });


    var doorSlider = {

      startOpenDistance : 550,
      stopOpenDistance : 220,
      slideDoor: function () {
        this.distanceToTop = $("#doorhandle-l").offset().top - $(window).scrollTop();
        this.open = this.startOpenDistance - this.distanceToTop;
        if(this.distanceToTop < this.startOpenDistance && this.distanceToTop > this.stopOpenDistance) {
          $('#doorhandle-l').css('right', this.open/2 + 'px' );
          $('#doorhandle-r').css('left', this.open/2 + 'px' );
        }else if(this.distanceToTop <= this.stopOpenDistance) {
          $('#doorhandle-l').css('right','170px' );
          $('#doorhandle-r').css('left', '170px' );
        }else if(this.distanceToTop >= this.startOpenDistance) {
          $('#doorhandle-l').css('right', '0' );
          $('#doorhandle-r').css('left', '0' );
        }
      },
      init: function() {
        var that = this;
        $(window).scroll(function() {
          that.slideDoor();
        });
      }
    };

    doorSlider.init();


    var getPartials = {


      init: function() {
        $('#villa-types-carousel .item:not(:first)').each(function(e) {
          var $this = $(this);
          var retryCount = 0;
          $this.data('retryCount' , 0);
          var url = $this.attr('data-content-source') + '?ajax=1';
          //check if ajax loaded already
          if ( $this.is('[data-ajax-loaded]') === false ) {
            getPartials.getData($this, url);
          }
        });

      },

      getData: function(item, url) {

          var jqxhr = $.get(url, function(data){
            getPartials.callBack(item, data);
          }).done(function() {

          }).fail(function(xhr, textStatus, errorThrown){
            item.data('retryCount', item.data('retryCount') + 1 );
          //console.log(textStatus + ' plus ' + errorThrown);
            if(item.data('retryCount') <=5) setTimeout(getPartials.getData(item, url), 1500);
          });
      },

      callBack: function(item, data) {

        item.attr('data-ajax-loaded','').html(data);

        $('.villa-types-image-zoom a').magnificPopup({
          type: 'image',
          closeOnContentClick: true,
          closeBtnInside: false,
          fixedContentPos: true,
          mainClass: 'mfp-no-margins mfp-with-zoom', // class to remove default margin from left and right side
          image: {
            verticalFit: true
          },
          zoom: {
            enabled: true,
            duration: 300 // don't foget to change the duration also in CSS
          }
        });
      }
    };

    getPartials.init();



    $('.villa-types').click(function(e) {
      e.preventDefault();
      villaTypesCarousel.carousel($(this).index() + 1);

    });


    //showing villa bullets after initial slide
    $('#villa-types-carousel').on('slide.bs.carousel', function (e) {
      //stupid event fires before new active class is attached to next element
      //http://stackoverflow.com/questions/9860436/twitter-bootstrap-carousel-access-current-index
      var nextSlideIndex = $(e.relatedTarget).index();
      if(nextSlideIndex === 0 ) {
        $('.villa-types-bullets').hide();
      }else {
        $('.villa-types-bullets').show();
      }
    });


    $('#benefits-tabs a').click(function (e) {
      e.preventDefault();
      $(this).tab('show');
    });
    $('#benefits-tabs a[href="#exclusive-tab"]').tab('show'); // Select first tab

  });


})(jQuery,this);


