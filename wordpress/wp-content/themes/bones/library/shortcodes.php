<?php

function accordion($atts, $content = "")
{
    extract(shortcode_atts(array(
        'title' => '',
        'name' => '',
        'extra' => '',
        'parent' => '',
        'open' => false
    ), $atts));


    //$id = str_replace(' ','-', strtolower($title));
    $id = uniqid('id');

    if(isset($open) && $open == true) {
      $panel_class = 'in';
      $link_class = '';
    }else {
      $panel_class = '';
      $link_class = 'collapsed';
    }

    //return '<div class="text type ' . $type . '" ' . $data . '><span class="text">' . do_shortcode($content) . '</span></div>';
    return  '<div class="panel panel-default">
                <div class="panel-heading">
                  <h4 class="panel-title">
                    <a class="panel-title-link '.$link_class.'" data-parent="#'.$parent.'" data-toggle="collapse" data-parent="" href="#'.$id.'">
                      '.$title.' <div style="float:right;"><span style="padding-right: 50px;">'.$extra.'</span><span class="caret"></span></div>
                    </a>
                  </h4>
                </div>
                <div id="'.$id.'" class="panel-collapse collapse '. $panel_class .'">
                  <div class="panel-body">
                    '. do_shortcode($content) .'
                  </div>
                </div>
              </div>';
}

add_shortcode('accordion', 'accordion');


function accordion_group($atts, $content = "")
{
    extract(shortcode_atts(array(
        'id' => ''
    ), $atts));

//<div class="panel-group" id="accordion">
    //return '<div class="text type ' . $type . '" ' . $data . '><span class="text">' . do_shortcode($content) . '</span></div>';
    return  '<div class="panel-group" id="'.$id.'">'. do_shortcode($content) .'</div>';
}

add_shortcode('accordiongroup', 'accordion_group');