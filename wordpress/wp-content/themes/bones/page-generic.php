<?php
/*
Template Name: Centered Content Box Template
*/
?>

<?php get_header(); ?>


<div id="main" role="main">
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
  <section class="container content">
      <div class="row">

      <div class="content-box col-xs-12 col-sm-6 col-sm-offset-3">
        <?php the_content(); ?>
      </div>
    </div>
  </section>
</div>
<?php endwhile; else : ?>
   <h1><?php _e( 'Oops, Post Not Found!', 'bonestheme' ); ?></h1>
<?php endif; ?>


<?php get_footer(); ?>
