<?php
/*
Template Name: About Lijiang Template
*/
?>

<?php get_header(); ?>


<div id="main" role="main">

    <?php
    $fp_sections = new WP_Query(array('post_type' => 'fp_section', 'order' => 'ASC', 'orderby' => 'menu_order', 'posts_per_page' => -1));

    while ($fp_sections->have_posts()) : $fp_sections->the_post();

    $section_template_id = get_post_meta( $post->ID, 'section_template_id', true );

    ?>
    <?php

    switch ($section_template_id) {
      case 'about-lijiang':

    ?>
    <section id="<?php echo $section_template_id;/*$post->post_name;*/ ?>" class="slide about-lijiang-<?php echo $section_template_id?>">
      <div class="container content">
        <div class="row">
          <div class="content-box col-xs-12 col-sm-6 col-sm-offset-3">
            <?php the_content(); ?>
          </div>
        </div>
      </div>
    </section>
    <?php
        break;

      case 'neighbourhood':
    ?>
    <section id="<?php echo $section_template_id;/*$post->post_name;*/ ?>" class="slide about-lijiang-<?php echo $section_template_id?>">
      <div class="container content">
        <div class="row">
          <div class="content-box col-xs-12 col-sm-6 col-sm-offset-3">
            <div class="panel-group accordion-neighbourhood">
            <?php the_content(); ?>
            </div>
          </div>
        </div>
      </div>
    </section>
    <?php
        break;
    }
    ?>
  </section>

  <?php endwhile; ?> <!-- END THE LOOP -->
    <?php wp_reset_postdata(); ?>

</div>


<?php get_footer(); ?>
