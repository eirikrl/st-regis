<?php get_header();

if(isset($_GET['pageback'])) {
	$pageLink = ($_GET['pageback'] == 1) ? "" : "page/" .$_GET['pageback'] ."/";
}else{
	$pageLink = null;
}



?>
<div id="main" role="main">
  <section class="container content">

    <div class="row">
    	<div class="col-xs-12 col-md-6 col-md-offset-3">
			<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

				<article class="news-article" id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">

					<header class="article-header">

						<h1 class="entry-title single-title" itemprop="headline"><?php the_title(); ?></h1>
						<p class="byline vcard"><?php
							printf( __( 'Posted <time class="updated" datetime="%1$s" pubdate>%2$s</time>', 'bonestheme' ), get_the_time( 'Y-m-j' ), get_the_time( get_option('date_format')), bones_get_the_author_posts_link(), get_the_category_list(', ') );
						?></p>

					</header>

					<section class="entry-content clearfix" itemprop="articleBody">
						<?php

						the_post_thumbnail('full', array('style' => 'margin-bottom: 10px;'));
						the_content();
						?>
					</section>

					<footer class="article-footer">
						<?php the_tags( '<p class="tags"><span class="tags-title">' . __( 'Tags:', 'bonestheme' ) . '</span> ', ', ', '</p>' ); ?>
						<div>
							<a href="../category/<?php translation_strings('news','news-zh'); ?>/<?php echo $pageLink ?>">Back to News</a>
						</div>
					</footer>

					<!-- <?php comments_template(); ?> -->

				</article>

			<?php endwhile; ?>

			<?php else : ?>

				<article id="post-not-found" class="hentry clearfix">
						<header class="article-header">
							<h1><?php _e( 'Oops, Post Not Found!', 'bonestheme' ); ?></h1>
						</header>
						<section class="entry-content">
							<p><?php _e( 'Uh Oh. Something is missing. Try double checking things.', 'bonestheme' ); ?></p>
						</section>
						<footer class="article-footer">
								<p><?php _e( 'This is the error message in the single.php template.', 'bonestheme' ); ?></p>
						</footer>
				</article>

			<?php endif; ?>
			</div>
		</div>
	</section>
</div>

<?php get_footer(); ?>
