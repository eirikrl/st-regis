<?php
/*
Author: Eddie Machado
URL: htp://themble.com/bones/

This is where you can drop your custom functions or
just edit things like thumbnail sizes, header images,
sidebars, comments, ect.
*/

/************* INCLUDE NEEDED FILES ***************/

/*
1. library/bones.php
	- head cleanup (remove rsd, uri links, junk css, ect)
	- enqueueing scripts & styles
	- theme support functions
	- custom menu output & fallbacks
	- related post function
	- page-navi function
	- removing <p> from around images
	- customizing the post excerpt
	- custom google+ integration
	- adding custom fields to user profiles
*/
require_once( 'library/bones.php' ); // if you remove this, bones will break
/*
2. library/custom-post-type.php
	- an example custom post type
	- example custom taxonomy (like categories)
	- example custom taxonomy (like tags)
*/
require_once( 'library/custom-post-type.php' ); // you can disable this if you like

/*
3. library/admin.php
	- removing some default WordPress dashboard widgets
	- an example custom dashboard widget
	- adding custom login css
	- changing text in footer of admin
*/
// require_once( 'library/admin.php' ); // this comes turned off by default
/*
4. library/translation/translation.php
	- adding support for other languages
*/
// require_once( 'library/translation/translation.php' ); // this comes turned off by default

//include 'library/meta-box-demo.php';

include 'library/shortcodes.php';
// add_filter('widget_text', 'shortcode_unautop');
// add_filter('widget_text', 'do_shortcode');

/************* THUMBNAIL SIZE OPTIONS *************/

// Thumbnail sizes
add_image_size( 'bones-thumb-600', 600, 150, true );
add_image_size( 'bones-thumb-300', 300, 100, true );

/*
to add more sizes, simply copy a line from above
and change the dimensions & name. As long as you
upload a "featured image" as large as the biggest
set width or height, all the other sizes will be
auto-cropped.

To call a different size, simply change the text
inside the thumbnail function.

For example, to call the 300 x 300 sized image,
we would use the function:
<?php the_post_thumbnail( 'bones-thumb-300' ); ?>
for the 600 x 100 image:
<?php the_post_thumbnail( 'bones-thumb-600' ); ?>

You can change the names and dimensions to whatever
you like. Enjoy!
*/

require_once('plugins/gallery/gallery.php');

//utility function for translation strings

function translation_strings($en, $zh, $echo = TRUE) {
  if(function_exists('pll_current_language')) {

    if(pll_current_language() == 'zh') {
      if($echo == TRUE) {
        echo $zh;
      }else{
        return $zh;
      }
    }else{
      if($echo == TRUE) {
        echo $en;
      }else{
        return $en;
      }
    }

  }else {

    if($echo == TRUE) {
      echo $en;
    }else{
      return $en;
    }
  }
}

function get_language() {

  if(function_exists('pll_current_language')) {

     return pll_current_language();
  }else {

    return 'en';
  }
}

add_filter( 'image_size_names_choose', 'bones_custom_image_sizes' );

function bones_custom_image_sizes( $sizes ) {
    return array_merge( $sizes, array(
        'bones-thumb-600' => __('600px by 150px'),
        'bones-thumb-300' => __('300px by 100px'),
    ) );
}

/*
The function above adds the ability to use the dropdown menu to select
the new images sizes you have just created from within the media manager
when you add media to your content blocks. If you add more image sizes,
duplicate one of the lines in the array and name it according to your
new image size.
*/

/************* ACTIVE SIDEBARS ********************/

// Sidebars & Widgetizes Areas
function bones_register_sidebars() {
	register_sidebar(array(
		'id' => 'sidebar1',
		'name' => __( 'Sidebar 1', 'bonestheme' ),
		'description' => __( 'The first (primary) sidebar.', 'bonestheme' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h4 class="widgettitle">',
		'after_title' => '</h4>',
	));

	/*
	to add more sidebars or widgetized areas, just copy
	and edit the above sidebar code. In order to call
	your new sidebar just use the following code:

	Just change the name to whatever your new
	sidebar's id is, for example:

	register_sidebar(array(
		'id' => 'sidebar2',
		'name' => __( 'Sidebar 2', 'bonestheme' ),
		'description' => __( 'The second (secondary) sidebar.', 'bonestheme' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h4 class="widgettitle">',
		'after_title' => '</h4>',
	));

	To call the sidebar in your template, you can just copy
	the sidebar.php file and rename it to your sidebar's name.
	So using the above example, it would be:
	sidebar-sidebar2.php

	*/
} // don't remove this bracket!

/************* COMMENT LAYOUT *********************/

// Comment Layout
function bones_comments( $comment, $args, $depth ) {
   $GLOBALS['comment'] = $comment; ?>
	<li <?php comment_class(); ?>>
		<article id="comment-<?php comment_ID(); ?>" class="clearfix">
			<header class="comment-author vcard">
				<?php
				/*
					this is the new responsive optimized comment image. It used the new HTML5 data-attribute to display comment gravatars on larger screens only. What this means is that on larger posts, mobile sites don't have a ton of requests for comment images. This makes load time incredibly fast! If you'd like to change it back, just replace it with the regular wordpress gravatar call:
					echo get_avatar($comment,$size='32',$default='<path_to_url>' );
				*/
				?>
				<?php // custom gravatar call ?>
				<?php
					// create variable
					$bgauthemail = get_comment_author_email();
				?>
				<img data-gravatar="http://www.gravatar.com/avatar/<?php echo md5( $bgauthemail ); ?>?s=32" class="load-gravatar avatar avatar-48 photo" height="32" width="32" src="<?php echo get_template_directory_uri(); ?>/library/images/nothing.gif" />
				<?php // end custom gravatar call ?>
				<?php printf(__( '<cite class="fn">%s</cite>', 'bonestheme' ), get_comment_author_link()) ?>
				<time datetime="<?php echo comment_time('Y-m-j'); ?>"><a href="<?php echo htmlspecialchars( get_comment_link( $comment->comment_ID ) ) ?>"><?php comment_time(__( 'F jS, Y', 'bonestheme' )); ?> </a></time>
				<?php edit_comment_link(__( '(Edit)', 'bonestheme' ),'  ','') ?>
			</header>
			<?php if ($comment->comment_approved == '0') : ?>
				<div class="alert alert-info">
					<p><?php _e( 'Your comment is awaiting moderation.', 'bonestheme' ) ?></p>
				</div>
			<?php endif; ?>
			<section class="comment_content clearfix">
				<?php comment_text() ?>
			</section>
			<?php comment_reply_link(array_merge( $args, array('depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
		</article>
	<?php // </li> is added by WordPress automatically ?>
<?php
} // don't remove this bracket!

/************* SEARCH FORM LAYOUT *****************/

// Search Form
function bones_wpsearch($form) {
	$form = '<form role="search" method="get" id="searchform" action="' . home_url( '/' ) . '" >
	<label class="screen-reader-text" for="s">' . __( 'Search for:', 'bonestheme' ) . '</label>
	<input type="text" value="' . get_search_query() . '" name="s" id="s" placeholder="' . esc_attr__( 'Search the Site...', 'bonestheme' ) . '" />
	<input type="submit" id="searchsubmit" value="' . esc_attr__( 'Search' ) .'" />
	</form>';
	return $form;
} // don't remove this bracket!






/************* CUSTOM META BOXES *****************/

//adding JS script to show/hide metaboxes
add_action('admin_enqueue_scripts', 'meta_boxes_script');

function meta_boxes_script() {
    wp_enqueue_script('my-admin',  get_stylesheet_directory_uri() .'/library/js/admin/meta-boxes.js', array('jquery'));
}

/* FP-SECTION templating

/* Fire our meta box setup function on the post editor screen. */
add_action( 'load-post.php', 'FP_template_id_meta_boxes_setup' );
add_action( 'load-post-new.php', 'FP_template_id_meta_boxes_setup' );
/* Meta box setup function. */
function FP_template_id_meta_boxes_setup() {

	/* Add meta boxes on the 'add_meta_boxes' hook. */
	add_action( 'add_meta_boxes', 'add_post_meta_boxes' );
	/* Save post meta on the 'save_post' hook. */
	add_action( 'save_post', 'save_post_class_meta', 10, 2 );
}
/* Create one or more meta boxes to be displayed on the post editor screen. */
function add_post_meta_boxes() {

	add_meta_box(
		'section-template',			// Unique ID
		esc_html__( 'Section Template ID', 'example' ),		// Title
		'template_id_meta_box',		// Callback function
		'fp_section',					// Admin page (or post type) i.e. post, page, etc
		'side',					// Context
		'default'					// Priority
	);
}

function checkSelected ($templateId, $object) {

	if( $templateId == esc_attr( get_post_meta( $object->ID, 'section_template_id', true )) ) {
		echo "selected=''";
	}
}
/* Display the post meta box. */
function template_id_meta_box( $object, $box ) { ?>

	<?php wp_nonce_field( basename( __FILE__ ), 'section_template_id_nonce' ); ?>
	<p>
		<label for="section-template-id"><?php _e( "Select section template ID", 'example' ); ?></label>
		<br />
		<!--input class="widefat" type="text" name="section-template-id" id="section-template-id" value="<?php echo esc_attr( get_post_meta( $object->ID, 'section-template-id', true ) ); ?>" size="30" /-->

		<select class="widefat"  name="section-template-id" id="section-template-id">
			<option value="">Select template ID</option>
			<option <?php checkSelected('intro', $object); ?> value="intro">intro</option>
			<option <?php checkSelected('overview', $object); ?> value="overview">overview</option>
			<option <?php checkSelected('location', $object); ?> value="location">location</option>
			<option <?php checkSelected('villa-types', $object); ?> value="villa-types">villa-types</option>
			<option <?php checkSelected('benefits', $object); ?> value="benefits">benefits</option>
      <option <?php checkSelected('about-lijiang', $object); ?> value="about-lijiang">about-lijiang</option>
      <option <?php checkSelected('neighbourhood', $object); ?> value="neighbourhood">neighourhood</option>
		</select>

	</p>
<?php
}

/* Save the meta box's post metadata. */
function save_post_class_meta( $post_id, $post ) {

	/* Verify the nonce before proceeding. */
	if ( !isset( $_POST['section_template_id_nonce'] ) || !wp_verify_nonce( $_POST['section_template_id_nonce'], basename( __FILE__ ) ) )
		return $post_id;

	/* Get the post type object. */
	$post_type = get_post_type_object( $post->post_type );

	/* Check if the current user has permission to edit the post. */
	if ( !current_user_can( $post_type->cap->edit_post, $post_id ) )
		return $post_id;

	/* Get the posted data and sanitize it for use as an HTML class. */
	$new_meta_value = ( isset( $_POST['section-template-id'] ) ? sanitize_html_class( $_POST['section-template-id'] ) : '' );

	/* Get the meta key. */
	$meta_key = 'section_template_id';

	/* Get the meta value of the custom field key. */
	$meta_value = get_post_meta( $post_id, $meta_key, true );

	/* If a new meta value was added and there was no previous value, add it. */
	if ( $new_meta_value && '' == $meta_value )
		add_post_meta( $post_id, $meta_key, $new_meta_value, true );

	/* If the new meta value does not match the old value, update it. */
	elseif ( $new_meta_value && $new_meta_value != $meta_value )
		update_post_meta( $post_id, $meta_key, $new_meta_value );

	/* If there is no new meta value but an old value exists, delete it. */
	elseif ( '' == $new_meta_value && $meta_value )
		delete_post_meta( $post_id, $meta_key, $meta_value );
}




/* switched to using RWMB plugin API for metabox creation below
see https://github.com/rilwis/meta-box */

/* villa type tooltip description */
add_filter( 'rwmb_meta_boxes', 'villa_type_register_meta_boxes' );

function villa_type_register_meta_boxes( $meta_boxes ) {
  /**
   * Prefix of meta keys (optional)
   * Use underscore (_) at the beginning to make keys hidden
   * Alt.: You also can make prefix empty to disable it
   */
  // Better has an underscore as last sign
  $prefix = '_villa_type_';


  // 2nd meta box
  $meta_boxes[] = array(

    // Meta box title - Will appear at the drag and drop handle bar. Required.
    'title' => __( 'Additional Info', 'rwmb' ),

    // Post types, accept custom post types as well - DEFAULT is array('post'). Optional.
    'pages' => array( 'villa_type' ),

    // Where the meta box appear: normal (default), advanced, side. Optional.
    'context' => 'normal',

    // Order of meta box: high (default), low. Optional.
    'priority' => 'high',

    // Auto save: true, false (default). Optional.
    'autosave' => true,

    'fields' => array(
      // WYSIWYG/RICH TEXT EDITOR
      array(
        'name' => __( 'Specs' ),
        'id'   => "{$prefix}specs",
        'type' => 'wysiwyg',
        // Set the 'raw' parameter to TRUE to prevent data being passed through wpautop() on save
        'raw'  => false,
        'std'  => __( '', 'rwmb' ),

        // Editor settings, see wp_editor() function: look4wp.com/wp_editor
        'options' => array(
          'textarea_rows' => 4,
          'teeny'         => true,
          'media_buttons' => false,
        )
      ),
      array(
        'name' => __( 'Basement Image', 'rwmb' ),
        'id'   => "{$prefix}floorplan_B",
        'type' => 'image',
      ),
      array(
        'name' => __( 'First Floor Image', 'rwmb' ),
        'id'   => "{$prefix}floorplan_1F",
        'type' => 'image',
      ),
      array(
        'name' => __( 'Second Floor Image', 'rwmb' ),
        'id'   => "{$prefix}floorplan_2F",
        'type' => 'image',
      )
    )
  );

  return $meta_boxes;
}




/* contact us disclaimer and address metabox */
add_filter( 'rwmb_meta_boxes', 'contact_us_register_meta_boxes' );

function contact_us_register_meta_boxes( $meta_boxes ) {

  $prefix = '_contact_us_';

  // 2nd meta box
  $meta_boxes[] = array(

    // Meta box title - Will appear at the drag and drop handle bar. Required.
    'title' => __( 'extra information', 'rwmb' ),

    // Post types, accept custom post types as well - DEFAULT is array('post'). Optional.
    'pages' => array('page'),

    // Where the meta box appear: normal (default), advanced, side. Optional.
    'context' => 'normal',

    // Order of meta box: high (default), low. Optional.
    'priority' => 'high',

    // Auto save: true, false (default). Optional.
    'autosave' => true,

    'fields' => array(
      // WYSIWYG/RICH TEXT EDITOR
      array(
        'name' => __( 'Disclaimer' ),
        'id'   => "{$prefix}disclaimer",
        'type' => 'wysiwyg',
        // Set the 'raw' parameter to TRUE to prevent data being passed through wpautop() on save
        'raw'  => false,
        'std'  => __( '', 'rwmb' ),

        // Editor settings, see wp_editor() function: look4wp.com/wp_editor
        'options' => array(
          'textarea_rows' => 4,
          'teeny'         => true,
          'media_buttons' => false,
        ),
      ),
      array(
        'name' => __( 'Address' ),
        'id'   => "{$prefix}address",
        'type' => 'textarea',
        'cols' => 20,
        'rows' => 3,
      ),
      array(
        'name' => __( 'Telephone', 'rwmb' ),
        'id'   => "{$prefix}telephone",
        'type' => 'text',
      ),
      array(
        'name' => __( 'Email', 'rwmb' ),
        'id'   => "{$prefix}email",
        'type' => 'text',
      ),
    )
  );

  return $meta_boxes;
}


/* contact us disclaimer metabox */
add_filter( 'rwmb_meta_boxes', 'benefits_register_meta_boxes' );

function benefits_register_meta_boxes( $meta_boxes ) {

  $prefix = '_benefits_';

  // 2nd meta box
  $meta_boxes[] = array(

    // Meta box title - Will appear at the drag and drop handle bar. Required.
    'title' => __( 'Benefits Tabs', 'rwmb' ),

    // Post types, accept custom post types as well - DEFAULT is array('post'). Optional.
    'pages' => array( 'fp_section' ),

    // Where the meta box appear: normal (default), advanced, side. Optional.
    'context' => 'normal',

    // Order of meta box: high (default), low. Optional.
    'priority' => 'high',

    // Auto save: true, false (default). Optional.
    'autosave' => true,

    'fields' => array(
      // WYSIWYG/RICH TEXT EDITOR
      array(
        'name' => __( 'Exclusive Benefits Tab' ),
        'id'   => "{$prefix}exclusive",
        'type' => 'wysiwyg',
        // Set the 'raw' parameter to TRUE to prevent data being passed through wpautop() on save
        'raw'  => false,
        'std'  => __( '', 'rwmb' ),

        // Editor settings, see wp_editor() function: look4wp.com/wp_editor
        'options' => array(
          'textarea_rows' => 4,
          'teeny'         => true,
          'media_buttons' => false,
        ),
      ),
      array(
        'name' => __( 'Customized Benefits Tab' ),
        'id'   => "{$prefix}customized",
        'type' => 'wysiwyg',
        // Set the 'raw' parameter to TRUE to prevent data being passed through wpautop() on save
        'raw'  => false,
        'std'  => __( '', 'rwmb' ),

        // Editor settings, see wp_editor() function: look4wp.com/wp_editor
        'options' => array(
          'textarea_rows' => 4,
          'teeny'         => true,
          'media_buttons' => false,
        ),
      )

    )
  );

  return $meta_boxes;
}



//http://wordpress.stackexchange.com/questions/91202/replace-current-page-item-class-in-menu
function add_active_class_to_nav_menu($classes) {
    if (in_array('current-menu-item', $classes, true) || in_array('current_page_item', $classes, true)) {
        $classes = array_diff($classes, array('current-menu-item', 'current_page_item', 'active'));
        $classes[] = 'active';
    }
    return $classes;
}


/***** WALKER CLASS FOR CUSTOM NAV *****/


class CustomMenuWalker extends Walker_Nav_Menu {

	// add classes to ul sub-menus
	function start_lvl( &$output, $depth ) {
	    // depth dependent classes
	    $indent = ( $depth > 0  ? str_repeat( "\t", $depth ) : '' ); // code indent
	    $display_depth = ( $depth + 1); // because it counts the first submenu as 0
	    $classes = array(
	        'navbar-subnav', 'hidden-xs' , 'nav'
	        //( $display_depth % 2  ? 'menu-odd' : 'menu-even' ),
	        //( $display_depth >=2 ? 'sub-sub-menu' : '' ),
	        //'menu-depth-' . $display_depth
	        );
	    $class_names = implode( ' ', $classes );

	    // build html
	    $output .= "\n" . $indent . '<ul class="' . $class_names . '">' . "\n";
	}

	// add main/sub classes to li's and links
	function start_el( &$output, $item, $depth, $args ) {
	  global $wp_query;
	  $indent = ( $depth > 0 ? str_repeat( "\t", $depth ) : '' ); // code indent

	  //depth dependent classes
	  $depth_classes = array(
	      ( $depth == 0 ? 'main-menu-item' : 'sub-menu-item' ),
	      ( $depth >=2 ? 'sub-sub-menu-item' : '' ),
	      ( $depth % 2 ? 'menu-item-odd' : 'menu-item-even' ),
	      'menu-item-depth-' . $depth
	  );

	  $depth_class_names = esc_attr( implode( ' ', $depth_classes ) );

	  add_filter('nav_menu_css_class', 'add_active_class_to_nav_menu');
	  // passed classes


	  $classes = empty( $item->classes ) ? array() : (array) $item->classes;

    $classes = apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item );
	  $class_names = esc_attr( implode( ' ', $classes) );

    //only attaching scrollspy-target to menu items with children and active class, because scrollspy automatically makes its targets active
	  $class_names .= ' '.(in_array('menu-item-has-children', $classes) && (in_array('active', $classes)) ? 'scrollspy-target' : '');

	  // build html
	  // if( $depth == 0) {
			// $output .= $indent . '<li id="nav-menu-item-'. $item->ID . '" class="' . $class_names . '">';
	  // }else if (  $depth == 1) {
			// $output .= $indent . '<li id="nav-menu-item-'. $item->ID . '" data-slide="'. str_replace("#", '', $item->url).'" class="' . $class_names . '">';
	  // }

		$output .= $indent . '<li id="nav-menu-item-'. $item->ID.'" ' .($depth == 1? 'data-section="'. str_replace("#", '', $item->url).'"': ""). ' class="' . $class_names .'"    >';

	  // link attributes
	  $attributes  = ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';

	  $attributes  = ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';
	  $attributes .= ! empty( $item->target )     ? ' target="' . esc_attr( $item->target     ) .'"' : '';
	  $attributes .= ! empty( $item->xfn )        ? ' rel="'    . esc_attr( $item->xfn        ) .'"' : '';
	  $attributes .= ! empty( $item->url )        ? ' href="'   . esc_attr( $item->url        ) .'"' : '';
	  $attributes .= ' class="menu-link ' . ( $depth > 0 ? 'sub-menu-link' : 'main-menu-link' ) . '"';

	  $item_output = sprintf( '%1$s<a%2$s>%3$s%4$s%5$s</a>%6$s',
	      $args->before,
	      $attributes,
	      $args->link_before,
	      apply_filters( 'the_title', $item->title, $item->ID ),
	      $args->link_after,
	      $args->after
	  );

	  // build html
	  $output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );

	  remove_filter('nav_menu_css_class', 'add_active_class_to_nav_menu');
	}

}


?>
