<?php
/*
This is the custom post type post template.
If you edit the post type name, you've got
to change the name of this template to
reflect that name change.

i.e. if your custom post type is called
register_post_type( 'bookmarks',
then your single template should be
single-bookmarks.php

*/
?>

<?php
if( !isset($_GET['ajax']) || $_GET['ajax'] != 1) {
  get_header();
}
?>

    <?php
    if (have_posts()) : while (have_posts()) : the_post();
    $specs = get_post_meta( $post->ID, '_villa_type_specs', true );
    $basementImages = rwmb_meta( '_villa_type_floorplan_B', 'type=image' );
    $firstFloorImages = rwmb_meta( '_villa_type_floorplan_1F', 'type=image' );
    $secondFloorImages = rwmb_meta( '_villa_type_floorplan_2F', 'type=image' );
    $imgCount = count($basementImages) + count($firstFloorImages) + count($secondFloorImages);
    //images have the following keys
    /*
      array(
        'name' => 'logo-150x80.png',
        'path' => '/var/www/wp-content/uploads/logo-150x80.png',
        'url' => 'http://example.com/wp-content/uploads/logo-150x80.png',
        'width' => 150,
        'height' => 80,
        'full_url' => 'http://example.com/wp-content/uploads/logo.png',
        'title' => 'Logo',
        'caption' => 'Logo caption',
        'description' => 'Used in the header',
        'alt' => 'Logo ALT text',
    */


    ?>

      <section class="villa-types-detail <?php echo $post->post_name; ?>">
        <div class="background-dimmer"></div>
        <div class="container">
          <div class="row">
            <div class="col-md-3">
              <h1 class="villa-types-inner villa-types-inner-<?php echo $post->post_name; ?>">Shan Qu</h1>

              <div class="villa-types-highlight"><?php echo $specs; ?></div>
            </div>
            <div class="col-md-9">
              <div class="villa-types-detail-content">
                <div class="row villa-types-image-zoom">
                  <?php
                  foreach ( $basementImages as $image ) {
                    //echo "<a href='{$image['full_url']}' title='{$image['title']}' rel='thickbox'><img src='{$image['url']}' width='{$image['width']}' height='{$image['height']}' alt='{$image['alt']}' /></a>";
                   ?>
                   <div class="col-md-<?php echo 12/$imgCount; ?>">
                     <a href="<?php echo $image['full_url']; ?>" >
                      <figure>
                        <img src="<?php echo $image['full_url']; ?>" width="350"  />
                        <figcaption>
                        <span><?php translation_strings('Basement','地下室'); ?>&nbsp;<span class="glyphicon glyphicon-zoom-in"></span></span>
                        </figcaption>
                      </figure>
                    </a>
                  </div>
                  <?php
                  }

                  foreach ( $firstFloorImages as $image ) {
                   ?>
                   <div class="col-md-<?php echo 12/$imgCount; ?>">
                    <a href="<?php echo $image['full_url']; ?>">
                      <figure>
                        <img src="<?php echo $image['full_url']; ?>" width="350"  />
                        <figcaption>
                        <span><?php translation_strings('First floor','一楼'); ?>&nbsp;<span class="glyphicon glyphicon-zoom-in"></span></span>
                        </figcaption>
                    </figure>
                    </a>
                  </div>
                  <?php
                  }
                  foreach ( $secondFloorImages as $image ) {
                   ?>
                   <div class="col-md-<?php echo 12/$imgCount; ?>">
                      <a href="<?php echo $image['full_url']; ?>">
                      <figure>
                        <img src="<?php echo $image['full_url']; ?>" width="350"  />
                        <figcaption>
                        <span><?php translation_strings('Second floor','二楼'); ?>&nbsp;<span class="glyphicon glyphicon-zoom-in"></span></span>
                        </figcaption>
                      </figure>
                    </a>
                  </div>
                  <?php
                  }
                  ?>
                </div>
                <?php the_content(); ?>
              </div>

            </div>
          </div>
        </div>
      </section>



    <?php endwhile; ?>

    <?php else : ?>

        <article id="post-not-found" class="hentry clearfix">
          <header class="article-header">
            <h1><?php _e( 'Oops, Post Not Found!', 'bonestheme' ); ?></h1>
          </header>
          <section class="entry-content">
            <p><?php _e( 'Uh Oh. Something is missing. Try double checking things.', 'bonestheme' ); ?></p>
          </section>
          <footer class="article-footer">
              <p><?php _e( 'This is the error message in the single-custom_type.php template.', 'bonestheme' ); ?></p>
          </footer>
        </article>

    <?php endif; ?>


<?php
if( !isset($_GET['ajax']) && $_GET['ajax'] != 1) {
  get_footer();
}
?>
