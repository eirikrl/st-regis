<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'st-regis');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'lQF[0x#!$a7]s6/0+SV;p?<.iWnqFE? pM<XO+|:PFWhm?5Q}]H`7J+!|BjrCogg');
define('SECURE_AUTH_KEY',  'OUTcM3+-AAA_WUR|Df2_{(iOLYqK9<DdN>0>GaH.8sO__Y>k6mxpX8:Wk_.mlgRm');
define('LOGGED_IN_KEY',    'X[hdW0CBP}/%{cz9R1T,WGW1|R*Pq=czTV`^6@Vl&wp^0<R|4d4mHF05J?r1!fSm');
define('NONCE_KEY',        '+#>HiXA+=l}Y@E>C-L0{LVg$wpGG7Evfi`ut],R{UbH+SKDsKtlJUmg.84d!*Xjq');
define('AUTH_SALT',        'lo [fL/xx:J(!b|]^|<n4:tt ncaa:-wYez)&}*;2j[t1~@*$Nv=i8@3LYH 8x*(');
define('SECURE_AUTH_SALT', 'YkUH=Pk(.L[&7+-?wGVkmpkZ+uCT9u2$~Wn+_G%^,$m^&D2Px+Qxdj4ms]8!QgK|');
define('LOGGED_IN_SALT',   '<Cazc:?Cm2AA<T}Rxh$tT-bPk{ofJ +M $]1cK2 yNPb#-/4#n*k6g.y3<`GV=-D');
define('NONCE_SALT',       ':hh1xy.+}qbv=J}QRSvFlt+v.Cxd.42S)*`2jLxR=~*g8*wr8T1WXe2&KO}lyKj>');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
