/*! st-regis 24-01-2014 */
/**
 * Isotope v1.5.25
 * An exquisite jQuery plugin for magical layouts
 * http://isotope.metafizzy.co
 *
 * Commercial use requires one-time purchase of a commercial license
 * http://isotope.metafizzy.co/docs/license.html
 *
 * Non-commercial use is licensed under the MIT License
 *
 * Copyright 2013 Metafizzy
 */
/*jshint asi: true, browser: true, curly: true, eqeqeq: true, forin: false, immed: false, newcap: true, noempty: true, strict: true, undef: true */
/*global jQuery: false */
!function(a,b){"use strict";// get global vars
var c,d=a.document,e=a.Modernizr,f=function(a){return a.charAt(0).toUpperCase()+a.slice(1)},g="Moz Webkit O Ms".split(" "),h=function(a){var b,c=d.documentElement.style;// test standard property first
if("string"==typeof c[a])return a;// capitalize
a=f(a);// test vendor specific properties
for(var e=0,h=g.length;h>e;e++)if(b=g[e]+a,"string"==typeof c[b])return b},i=h("transform"),j=h("transitionProperty"),k={csstransforms:function(){return!!i},csstransforms3d:function(){var a=!!h("perspective");// double check for Chrome's false positive
if(a){var c=" -o- -moz- -ms- -webkit- -khtml- ".split(" "),d="@media ("+c.join("transform-3d),(")+"modernizr)",e=b("<style>"+d+"{#modernizr{height:3px}}</style>").appendTo("head"),f=b('<div id="modernizr" />').appendTo("html");a=3===f.height(),f.remove(),e.remove()}return a},csstransitions:function(){return!!j}};if(e)// if there's a previous Modernzir, check if there are necessary tests
for(c in k)e.hasOwnProperty(c)||// if test hasn't been run, use addTest to run it
e.addTest(c,k[c]);else{// or create new mini Modernizr that just has the 3 tests
e=a.Modernizr={_version:"1.6ish: miniModernizr for Isotope"};var l,m=" ";// Run through tests
for(c in k)l=k[c](),e[c]=l,m+=" "+(l?"":"no-")+c;// Add the new classes to the <html> element.
b("html").addClass(m)}// ========================= isoTransform ===============================
/**
   *  provides hooks for .css({ scale: value, translate: [x, y] })
   *  Progressively enhanced CSS transforms
   *  Uses hardware accelerated 3D transforms for Safari
   *  or falls back to 2D transforms.
   */
if(e.csstransforms){// i.e. transformFnNotations.scale(0.5) >> 'scale3d( 0.5, 0.5, 1)'
var n=e.csstransforms3d?{// 3D transform functions
translate:function(a){return"translate3d("+a[0]+"px, "+a[1]+"px, 0) "},scale:function(a){return"scale3d("+a+", "+a+", 1) "}}:{// 2D transform functions
translate:function(a){return"translate("+a[0]+"px, "+a[1]+"px) "},scale:function(a){return"scale("+a+") "}},o=function(a,c,d){// unpack current transform data
var e,f,g=b.data(a,"isoTransform")||{},h={},j={};// i.e. newData.scale = 0.5
h[c]=d,// extend new value over current data
b.extend(g,h);for(e in g)f=g[e],j[e]=n[e](f);// get proper order
// ideally, we could loop through this give an array, but since we only have
// a couple transforms we're keeping track of, we'll do it like so
var k=j.translate||"",l=j.scale||"",// sorting so translate always comes first
m=k+l;// set data back in elem
b.data(a,"isoTransform",g),// set name to vendor specific property
a.style[i]=m};// ==================== scale ===================
b.cssNumber.scale=!0,b.cssHooks.scale={set:function(a,b){// uncomment this bit if you want to properly parse strings
// if ( typeof value === 'string' ) {
//   value = parseFloat( value );
// }
o(a,"scale",b)},get:function(a){var c=b.data(a,"isoTransform");return c&&c.scale?c.scale:1}},b.fx.step.scale=function(a){b.cssHooks.scale.set(a.elem,a.now+a.unit)},// ==================== translate ===================
b.cssNumber.translate=!0,b.cssHooks.translate={set:function(a,b){// uncomment this bit if you want to properly parse strings
// if ( typeof value === 'string' ) {
//   value = value.split(' ');
// }
//
// var i, val;
// for ( i = 0; i < 2; i++ ) {
//   val = value[i];
//   if ( typeof val === 'string' ) {
//     val = parseInt( val );
//   }
// }
o(a,"translate",b)},get:function(a){var c=b.data(a,"isoTransform");return c&&c.translate?c.translate:[0,0]}}}// ========================= get transition-end event ===============================
var p,q;e.csstransitions&&(p={WebkitTransitionProperty:"webkitTransitionEnd",// webkit
MozTransitionProperty:"transitionend",OTransitionProperty:"oTransitionEnd otransitionend",transitionProperty:"transitionend"}[j],q=h("transitionDuration"));// ========================= smartresize ===============================
/*
   * smartresize: debounced resize event for jQuery
   *
   * latest version and complete README available on Github:
   * https://github.com/louisremi/jquery.smartresize.js
   *
   * Copyright 2011 @louis_remi
   * Licensed under the MIT license.
   */
var r,s=b.event,t=b.event.handle?"handle":"dispatch";s.special.smartresize={setup:function(){b(this).bind("resize",s.special.smartresize.handler)},teardown:function(){b(this).unbind("resize",s.special.smartresize.handler)},handler:function(a,b){// Save the context
var c=this,d=arguments;// set correct event type
a.type="smartresize",r&&clearTimeout(r),r=setTimeout(function(){s[t].apply(c,d)},"execAsap"===b?0:100)}},b.fn.smartresize=function(a){return a?this.bind("smartresize",a):this.trigger("smartresize",["execAsap"])},// ========================= Isotope ===============================
// our "Widget" object constructor
b.Isotope=function(a,c,d){this.element=b(c),this._create(a),this._init(d)};// styles of container element we want to keep track of
var u=["width","height"],v=b(a);b.Isotope.settings={resizable:!0,layoutMode:"masonry",containerClass:"isotope",itemClass:"isotope-item",hiddenClass:"isotope-hidden",hiddenStyle:{opacity:0,scale:.001},visibleStyle:{opacity:1,scale:1},containerStyle:{position:"relative",overflow:"hidden"},animationEngine:"best-available",animationOptions:{queue:!1,duration:800},sortBy:"original-order",sortAscending:!0,resizesContainer:!0,transformsEnabled:!0,itemPositionDataEnabled:!1},b.Isotope.prototype={// sets up widget
_create:function(a){this.options=b.extend({},b.Isotope.settings,a),this.styleQueue=[],this.elemCount=0;// get original styles in case we re-apply them in .destroy()
var c=this.element[0].style;this.originalStyle={};// keep track of container styles
var d=u.slice(0);for(var e in this.options.containerStyle)d.push(e);for(var f=0,g=d.length;g>f;f++)e=d[f],this.originalStyle[e]=c[e]||"";// apply container style from options
this.element.css(this.options.containerStyle),this._updateAnimationEngine(),this._updateUsingTransforms();// sorting
var h={"original-order":function(a,b){return b.elemCount++,b.elemCount},random:function(){return Math.random()}};this.options.getSortData=b.extend(this.options.getSortData,h),// need to get atoms
this.reloadItems(),// get top left position of where the bricks should be
this.offset={left:parseInt(this.element.css("padding-left")||0,10),top:parseInt(this.element.css("padding-top")||0,10)};// add isotope class first time around
var i=this;setTimeout(function(){i.element.addClass(i.options.containerClass)},0),// bind resize method
this.options.resizable&&v.bind("smartresize.isotope",function(){i.resize()}),// dismiss all click events from hidden events
this.element.delegate("."+this.options.hiddenClass,"click",function(){return!1})},_getAtoms:function(a){var b=this.options.itemSelector,// filter & find
c=b?a.filter(b).add(a.find(b)):a,// base style for atoms
d={position:"absolute"};// filter out text nodes
return c=c.filter(function(a,b){return 1===b.nodeType}),this.usingTransforms&&(d.left=0,d.top=0),c.css(d).addClass(this.options.itemClass),this.updateSortData(c,!0),c},// _init fires when your instance is first created
// (from the constructor above), and when you
// attempt to initialize the widget again (by the bridge)
// after it has already been initialized.
_init:function(a){this.$filteredAtoms=this._filter(this.$allAtoms),this._sort(),this.reLayout(a)},option:function(a){// change options AFTER initialization:
// signature: $('#foo').bar({ cool:false });
if(b.isPlainObject(a)){this.options=b.extend(!0,this.options,a);// trigger _updateOptionName if it exists
var c;for(var d in a)c="_update"+f(d),this[c]&&this[c]()}},// ====================== updaters ====================== //
// kind of like setters
_updateAnimationEngine:function(){var a,b=this.options.animationEngine.toLowerCase().replace(/[ _\-]/g,"");// set applyStyleFnName
switch(b){case"css":case"none":a=!1;break;case"jquery":a=!0;break;default:// best available
a=!e.csstransitions}this.isUsingJQueryAnimation=a,this._updateUsingTransforms()},_updateTransformsEnabled:function(){this._updateUsingTransforms()},_updateUsingTransforms:function(){var a=this.usingTransforms=this.options.transformsEnabled&&e.csstransforms&&e.csstransitions&&!this.isUsingJQueryAnimation;// prevent scales when transforms are disabled
a||(delete this.options.hiddenStyle.scale,delete this.options.visibleStyle.scale),this.getPositionStyles=a?this._translate:this._positionAbs},// ====================== Filtering ======================
_filter:function(a){var b=""===this.options.filter?"*":this.options.filter;if(!b)return a;var c=this.options.hiddenClass,d="."+c,e=a.filter(d),f=e;if("*"!==b){f=e.filter(b);var g=a.not(d).not(b).addClass(c);this.styleQueue.push({$el:g,style:this.options.hiddenStyle})}return this.styleQueue.push({$el:f,style:this.options.visibleStyle}),f.removeClass(c),a.filter(b)},// ====================== Sorting ======================
updateSortData:function(a,c){var d,e,f=this,g=this.options.getSortData;a.each(function(){d=b(this),e={};// get value for sort data based on fn( $elem ) passed in
for(var a in g)e[a]=c||"original-order"!==a?g[a](d,f):b.data(this,"isotope-sort-data")[a];// apply sort data to element
b.data(this,"isotope-sort-data",e)})},// used on all the filtered atoms
_sort:function(){var a=this.options.sortBy,b=this._getSorter,c=this.options.sortAscending?1:-1,d=function(d,e){var f=b(d,a),g=b(e,a);// fall back to original order if data matches
return f===g&&"original-order"!==a&&(f=b(d,"original-order"),g=b(e,"original-order")),(f>g?1:g>f?-1:0)*c};this.$filteredAtoms.sort(d)},_getSorter:function(a,c){return b.data(a,"isotope-sort-data")[c]},// ====================== Layout Helpers ======================
_translate:function(a,b){return{translate:[a,b]}},_positionAbs:function(a,b){return{left:a,top:b}},_pushPosition:function(a,b,c){b=Math.round(b+this.offset.left),c=Math.round(c+this.offset.top);var d=this.getPositionStyles(b,c);this.styleQueue.push({$el:a,style:d}),this.options.itemPositionDataEnabled&&a.data("isotope-item-position",{x:b,y:c})},// ====================== General Layout ======================
// used on collection of atoms (should be filtered, and sorted before )
// accepts atoms-to-be-laid-out to start with
layout:function(a,b){var c=this.options.layoutMode;// set the size of the container
if(// layout logic
this["_"+c+"Layout"](a),this.options.resizesContainer){var d=this["_"+c+"GetContainerSize"]();this.styleQueue.push({$el:this.element,style:d})}this._processStyleQueue(a,b),this.isLaidOut=!0},_processStyleQueue:function(a,c){// are we animating the layout arrangement?
// use plugin-ish syntax for css or animate
var d,f,g,h,i=this.isLaidOut?this.isUsingJQueryAnimation?"animate":"css":"css",j=this.options.animationOptions,k=this.options.onLayout;if(// default styleQueue processor, may be overwritten down below
f=function(a,b){b.$el[i](b.style,j)},this._isInserting&&this.isUsingJQueryAnimation)// if using styleQueue to insert items
f=function(a,b){// only animate if it not being inserted
d=b.$el.hasClass("no-transition")?"css":i,b.$el[d](b.style,j)};else if(c||k||j.complete){// has callback
var l=!1,// array of possible callbacks to trigger
m=[c,k,j.complete],n=this;if(g=!0,// trigger callback only once
h=function(){if(!l){for(var b,c=0,d=m.length;d>c;c++)b=m[c],"function"==typeof b&&b.call(n.element,a,n);l=!0}},this.isUsingJQueryAnimation&&"animate"===i)// add callback to animation options
j.complete=h,g=!1;else if(e.csstransitions){// get first non-empty jQ object
for(// detect if first item has transition
var o,r=0,s=this.styleQueue[0],t=s&&s.$el;!t||!t.length;){// HACK: sometimes styleQueue[i] is undefined
if(o=this.styleQueue[r++],!o)return;t=o.$el}// get transition duration of the first element in that object
// yeah, this is inexact
var u=parseFloat(getComputedStyle(t[0])[q]);u>0&&(f=function(a,b){b.$el[i](b.style,j).one(p,h)},g=!1)}}// process styleQueue
b.each(this.styleQueue,f),g&&h(),// clear out queue for next time
this.styleQueue=[]},resize:function(){this["_"+this.options.layoutMode+"ResizeChanged"]()&&this.reLayout()},reLayout:function(a){this["_"+this.options.layoutMode+"Reset"](),this.layout(this.$filteredAtoms,a)},// ====================== Convenience methods ======================
// ====================== Adding items ======================
// adds a jQuery object of items to a isotope container
addItems:function(a,b){var c=this._getAtoms(a);// add new atoms to atoms pools
this.$allAtoms=this.$allAtoms.add(c),b&&b(c)},// convienence method for adding elements properly to any layout
// positions items, hides them, then animates them back in <--- very sezzy
insert:function(a,b){// position items
this.element.append(a);var c=this;this.addItems(a,function(a){var d=c._filter(a);c._addHideAppended(d),c._sort(),c.reLayout(),c._revealAppended(d,b)})},// convienence method for working with Infinite Scroll
appended:function(a,b){var c=this;this.addItems(a,function(a){c._addHideAppended(a),c.layout(a),c._revealAppended(a,b)})},// adds new atoms, then hides them before positioning
_addHideAppended:function(a){this.$filteredAtoms=this.$filteredAtoms.add(a),a.addClass("no-transition"),this._isInserting=!0,// apply hidden styles
this.styleQueue.push({$el:a,style:this.options.hiddenStyle})},// sets visible style on new atoms
_revealAppended:function(a,b){var c=this;// apply visible style after a sec
setTimeout(function(){// enable animation
a.removeClass("no-transition"),// reveal newly inserted filtered elements
c.styleQueue.push({$el:a,style:c.options.visibleStyle}),c._isInserting=!1,c._processStyleQueue(a,b)},10)},// gathers all atoms
reloadItems:function(){this.$allAtoms=this._getAtoms(this.element.children())},// removes elements from Isotope widget
remove:function(a,b){// remove elements immediately from Isotope instance
this.$allAtoms=this.$allAtoms.not(a),this.$filteredAtoms=this.$filteredAtoms.not(a);// remove() as a callback, for after transition / animation
var c=this,d=function(){a.remove(),b&&b.call(c.element)};a.filter(":not(."+this.options.hiddenClass+")").length?(// if any non-hidden content needs to be removed
this.styleQueue.push({$el:a,style:this.options.hiddenStyle}),this._sort(),this.reLayout(d)):// remove it now
d()},shuffle:function(a){this.updateSortData(this.$allAtoms),this.options.sortBy="random",this._sort(),this.reLayout(a)},// destroys widget, returns elements and container back (close) to original style
destroy:function(){var a=this.usingTransforms,b=this.options;this.$allAtoms.removeClass(b.hiddenClass+" "+b.itemClass).each(function(){var b=this.style;b.position="",b.top="",b.left="",b.opacity="",a&&(b[i]="")});// re-apply saved container styles
var c=this.element[0].style;for(var d in this.originalStyle)c[d]=this.originalStyle[d];this.element.unbind(".isotope").undelegate("."+b.hiddenClass,"click").removeClass(b.containerClass).removeData("isotope"),v.unbind(".isotope")},// ====================== LAYOUTS ======================
// calculates number of rows or columns
// requires columnWidth or rowHeight to be set on namespaced object
// i.e. this.masonry.columnWidth = 200
_getSegments:function(a){var b,c=this.options.layoutMode,d=a?"rowHeight":"columnWidth",e=a?"height":"width",g=a?"rows":"cols",h=this.element[e](),// i.e. options.masonry && options.masonry.columnWidth
i=this.options[c]&&this.options[c][d]||// or use the size of the first item, i.e. outerWidth
this.$filteredAtoms["outer"+f(e)](!0)||// if there's no items, use size of container
h;b=Math.floor(h/i),b=Math.max(b,1),// i.e. this.masonry.cols = ....
this[c][g]=b,// i.e. this.masonry.columnWidth = ...
this[c][d]=i},_checkIfSegmentsChanged:function(a){var b=this.options.layoutMode,c=a?"rows":"cols",d=this[b][c];// return if updated cols/rows is not equal to previous
// update cols/rows
return this._getSegments(a),this[b][c]!==d},// ====================== Masonry ======================
_masonryReset:function(){// layout-specific props
this.masonry={},// FIXME shouldn't have to call this again
this._getSegments();var a=this.masonry.cols;for(this.masonry.colYs=[];a--;)this.masonry.colYs.push(0)},_masonryLayout:function(a){var c=this,d=c.masonry;a.each(function(){var a=b(this),//how many columns does this brick span
e=Math.ceil(a.outerWidth(!0)/d.columnWidth);if(e=Math.min(e,d.cols),1===e)// if brick spans only one column, just like singleMode
c._masonryPlaceBrick(a,d.colYs);else{// brick spans more than one column
// how many different places could this brick fit horizontally
var f,g,h=d.cols+1-e,i=[];// for each group potential horizontal position
for(g=0;h>g;g++)// make an array of colY values for that one group
f=d.colYs.slice(g,g+e),// and get the max value of the array
i[g]=Math.max.apply(Math,f);c._masonryPlaceBrick(a,i)}})},// worker method that places brick in the columnSet
//   with the the minY
_masonryPlaceBrick:function(a,b){// Find index of short column, the first from the left
for(var c=Math.min.apply(Math,b),d=0,e=0,f=b.length;f>e;e++)if(b[e]===c){d=e;break}// position the brick
var g=this.masonry.columnWidth*d,h=c;this._pushPosition(a,g,h);// apply setHeight to necessary columns
var i=c+a.outerHeight(!0),j=this.masonry.cols+1-f;for(e=0;j>e;e++)this.masonry.colYs[d+e]=i},_masonryGetContainerSize:function(){var a=Math.max.apply(Math,this.masonry.colYs);return{height:a}},_masonryResizeChanged:function(){return this._checkIfSegmentsChanged()},// ====================== fitRows ======================
_fitRowsReset:function(){this.fitRows={x:0,y:0,height:0}},_fitRowsLayout:function(a){var c=this,d=this.element.width(),e=this.fitRows;a.each(function(){var a=b(this),f=a.outerWidth(!0),g=a.outerHeight(!0);0!==e.x&&f+e.x>d&&(// if this element cannot fit in the current row
e.x=0,e.y=e.height),// position the atom
c._pushPosition(a,e.x,e.y),e.height=Math.max(e.y+g,e.height),e.x+=f})},_fitRowsGetContainerSize:function(){return{height:this.fitRows.height}},_fitRowsResizeChanged:function(){return!0},// ====================== cellsByRow ======================
_cellsByRowReset:function(){this.cellsByRow={index:0},// get this.cellsByRow.columnWidth
this._getSegments(),// get this.cellsByRow.rowHeight
this._getSegments(!0)},_cellsByRowLayout:function(a){var c=this,d=this.cellsByRow;a.each(function(){var a=b(this),e=d.index%d.cols,f=Math.floor(d.index/d.cols),g=(e+.5)*d.columnWidth-a.outerWidth(!0)/2,h=(f+.5)*d.rowHeight-a.outerHeight(!0)/2;c._pushPosition(a,g,h),d.index++})},_cellsByRowGetContainerSize:function(){return{height:Math.ceil(this.$filteredAtoms.length/this.cellsByRow.cols)*this.cellsByRow.rowHeight+this.offset.top}},_cellsByRowResizeChanged:function(){return this._checkIfSegmentsChanged()},// ====================== straightDown ======================
_straightDownReset:function(){this.straightDown={y:0}},_straightDownLayout:function(a){var c=this;a.each(function(){var a=b(this);c._pushPosition(a,0,c.straightDown.y),c.straightDown.y+=a.outerHeight(!0)})},_straightDownGetContainerSize:function(){return{height:this.straightDown.y}},_straightDownResizeChanged:function(){return!0},// ====================== masonryHorizontal ======================
_masonryHorizontalReset:function(){// layout-specific props
this.masonryHorizontal={},// FIXME shouldn't have to call this again
this._getSegments(!0);var a=this.masonryHorizontal.rows;for(this.masonryHorizontal.rowXs=[];a--;)this.masonryHorizontal.rowXs.push(0)},_masonryHorizontalLayout:function(a){var c=this,d=c.masonryHorizontal;a.each(function(){var a=b(this),//how many rows does this brick span
e=Math.ceil(a.outerHeight(!0)/d.rowHeight);if(e=Math.min(e,d.rows),1===e)// if brick spans only one column, just like singleMode
c._masonryHorizontalPlaceBrick(a,d.rowXs);else{// brick spans more than one row
// how many different places could this brick fit horizontally
var f,g,h=d.rows+1-e,i=[];// for each group potential horizontal position
for(g=0;h>g;g++)// make an array of colY values for that one group
f=d.rowXs.slice(g,g+e),// and get the max value of the array
i[g]=Math.max.apply(Math,f);c._masonryHorizontalPlaceBrick(a,i)}})},_masonryHorizontalPlaceBrick:function(a,b){// Find index of smallest row, the first from the top
for(var c=Math.min.apply(Math,b),d=0,e=0,f=b.length;f>e;e++)if(b[e]===c){d=e;break}// position the brick
var g=c,h=this.masonryHorizontal.rowHeight*d;this._pushPosition(a,g,h);// apply setHeight to necessary columns
var i=c+a.outerWidth(!0),j=this.masonryHorizontal.rows+1-f;for(e=0;j>e;e++)this.masonryHorizontal.rowXs[d+e]=i},_masonryHorizontalGetContainerSize:function(){var a=Math.max.apply(Math,this.masonryHorizontal.rowXs);return{width:a}},_masonryHorizontalResizeChanged:function(){return this._checkIfSegmentsChanged(!0)},// ====================== fitColumns ======================
_fitColumnsReset:function(){this.fitColumns={x:0,y:0,width:0}},_fitColumnsLayout:function(a){var c=this,d=this.element.height(),e=this.fitColumns;a.each(function(){var a=b(this),f=a.outerWidth(!0),g=a.outerHeight(!0);0!==e.y&&g+e.y>d&&(// if this element cannot fit in the current column
e.x=e.width,e.y=0),// position the atom
c._pushPosition(a,e.x,e.y),e.width=Math.max(e.x+f,e.width),e.y+=g})},_fitColumnsGetContainerSize:function(){return{width:this.fitColumns.width}},_fitColumnsResizeChanged:function(){return!0},// ====================== cellsByColumn ======================
_cellsByColumnReset:function(){this.cellsByColumn={index:0},// get this.cellsByColumn.columnWidth
this._getSegments(),// get this.cellsByColumn.rowHeight
this._getSegments(!0)},_cellsByColumnLayout:function(a){var c=this,d=this.cellsByColumn;a.each(function(){var a=b(this),e=Math.floor(d.index/d.rows),f=d.index%d.rows,g=(e+.5)*d.columnWidth-a.outerWidth(!0)/2,h=(f+.5)*d.rowHeight-a.outerHeight(!0)/2;c._pushPosition(a,g,h),d.index++})},_cellsByColumnGetContainerSize:function(){return{width:Math.ceil(this.$filteredAtoms.length/this.cellsByColumn.rows)*this.cellsByColumn.columnWidth}},_cellsByColumnResizeChanged:function(){return this._checkIfSegmentsChanged(!0)},// ====================== straightAcross ======================
_straightAcrossReset:function(){this.straightAcross={x:0}},_straightAcrossLayout:function(a){var c=this;a.each(function(){var a=b(this);c._pushPosition(a,c.straightAcross.x,0),c.straightAcross.x+=a.outerWidth(!0)})},_straightAcrossGetContainerSize:function(){return{width:this.straightAcross.x}},_straightAcrossResizeChanged:function(){return!0}},// ======================= imagesLoaded Plugin ===============================
/*!
   * jQuery imagesLoaded plugin v1.1.0
   * http://github.com/desandro/imagesloaded
   *
   * MIT License. by Paul Irish et al.
   */
// $('#my-container').imagesLoaded(myFunction)
// or
// $('img').imagesLoaded(myFunction)
// execute a callback when all images have loaded.
// needed because .load() doesn't work on cached images
// callback function gets image collection as argument
//  `this` is the container
b.fn.imagesLoaded=function(a){function c(){a.call(e,f)}function d(a){var e=a.target;e.src!==h&&-1===b.inArray(e,i)&&(i.push(e),--g<=0&&(setTimeout(c),f.unbind(".imagesLoaded",d)))}var e=this,f=e.find("img").add(e.filter("img")),g=f.length,h="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///ywAAAAAAQABAAACAUwAOw==",i=[];// if no images, trigger immediately
return g||c(),f.bind("load.imagesLoaded error.imagesLoaded",d).each(function(){// cached images don't fire load sometimes, so we reset src.
var a=this.src;// webkit hack from http://groups.google.com/group/jquery-dev/browse_thread/thread/eee6ab7b2da50e1f
// data uri bypasses webkit log warning (thx doug jones)
this.src=h,this.src=a}),e};// helper function for logging errors
// $.error breaks jQuery chaining
var w=function(b){a.console&&a.console.error(b)};// =======================  Plugin bridge  ===============================
// leverages data method to either create or return $.Isotope constructor
// A bit from jQuery UI
//   https://github.com/jquery/jquery-ui/blob/master/ui/jquery.ui.widget.js
// A bit from jcarousel
//   https://github.com/jsor/jcarousel/blob/master/lib/jquery.jcarousel.js
b.fn.isotope=function(a,c){if("string"==typeof a){// call method
var d=Array.prototype.slice.call(arguments,1);this.each(function(){var c=b.data(this,"isotope");return c?b.isFunction(c[a])&&"_"!==a.charAt(0)?(// apply method
c[a].apply(c,d),void 0):(w("no such method '"+a+"' for isotope instance"),void 0):(w("cannot call methods on isotope prior to initialization; attempted to call method '"+a+"'"),void 0)})}else this.each(function(){var d=b.data(this,"isotope");d?(// apply options & init
d.option(a),d._init(c)):// initialize new instance
b.data(this,"isotope",new b.Isotope(a,this,c))});// return jQuery object
// so plugin methods do not have to
return this}}(window,jQuery);