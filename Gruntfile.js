module.exports = function(grunt) {

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    // concat: {
    //   options: {
    //     separator: ';'
    //   },
    //   dist: {
    //     src: ['src/**/*.js'],
    //     dest: 'dist/<%= pkg.name %>.js'
    //   }
    // },
    uglify: {
      options: {
        banner: '/*! <%= pkg.name %> <%= grunt.template.today("dd-mm-yyyy") %> */\n',
        preserveComments: 'all'
      },
      dist: {
        // files: {
        //   //'dist/<%= pkg.name %>.min.js': ['<%= concat.dist.dest %>']
        //   'js/script.min.js' : ['js/script.js']
        // }
        files: [{
          expand: true,
          cwd: 'js',
          src: '*.js',
          dest: 'dest/js'
        },
        {
          expand: true,
          cwd: 'js/libs',
          src: '*.js',
          dest: 'dest/js/libs'
        }
        ]
      }
    },
    // jshint: {
    //   files: ['Gruntfile.js', 'src/**/*.js', 'test/**/*.js'],
    //   options: {
    //     // options here to override JSHint defaults
    //     globals: {
    //       jQuery: true,
    //       console: true,
    //       module: true,
    //       document: true
    //     }
    //   }
    // },
    // watch: {
    //   files: ['<%= jshint.files %>'],
    //   tasks: ['jshint', 'qunit']
    // },
    less: {
      development: {
        options: {
          paths: ["css/less"]
        },
        files: {
          "css/styles.css": "css/less/styles.less"
          //"wordpress/wp-content/themes/bones/library/css/style.css" : "css/less/styles.less"
        }
      }
      //,
      // production: {
      //   options: {
      //     paths: ["assets/css"],
      //     cleancss: true
      //   },
      //   files: {
      //     "css/result.css": "css/less/styles.less"
      //   }
      // }
    }

  });

  grunt.loadNpmTasks('grunt-contrib-uglify');
  //grunt.loadNpmTasks('grunt-contrib-jshint');
  //grunt.loadNpmTasks('grunt-contrib-watch');
  //grunt.loadNpmTasks('grunt-contrib-concat');

  grunt.loadNpmTasks('grunt-contrib-less');

  //grunt.registerTask('test', ['jshint', 'qunit']);

  grunt.registerTask('default', ['uglify','less']);

};